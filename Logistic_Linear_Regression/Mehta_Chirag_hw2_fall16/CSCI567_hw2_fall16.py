'''
Created on Sep 27, 2016

@author: chira
'''

import copy
import itertools

from numpy import matrix as matrix
import numpy
import scipy.stats
from scipy.stats.stats import pearsonr as pearson
from sklearn.datasets import load_boston

import matplotlib.pyplot as pt
import random as ran


def get_pearson_coefficients():
    p_values = []
    
    for i in range(len(attr_array)):
        p_values.append(round(pearson(attr_array[i], train_y)[0],2))
    
    print 'Pearson Coefficents:',p_values


def get_mean_median():
    
    mean = numpy.mean(train_x, axis = 0)
    std = numpy.std(train_x, axis=0, ddof = 1)
    
    return mean, std


def normalize_train_data():
    
    for values in train_x:
        for i in range(len(values)):
            values[i] = (values[i] - mean_arr[i]) / std_arr[i]

def normalize_test_data():
    
    for values in test_x:
        for i in range(len(values)):
            values[i] = (values[i] - mean_arr[i]) / std_arr[i]

def get_linear_weights():
    
    x_bar = [[1]+ i for i in train_x]

    x = numpy.array(x_bar)
    x_t = matrix.transpose(x)
    
    x_x_t = numpy.dot(x_t, x)
    y = numpy.array(train_y)
    x_y_t = numpy.dot(x_t, y)
    
    w = numpy.linalg.solve(x_x_t, x_y_t)
    
    return w

def get_mean_square_error_train_linear():
     
    err_sum = 0
 
    for index in range(len(train_x)):
        y_pred = weights[0]
        for i in range(13):
            y_pred += weights[i+1]*train_x[index][i]
         
        err_sum += (train_y[index] - y_pred)**2
    
    print
    print 'Mean Squared Error - Training:',(err_sum / len(train_x))
    
def get_mean_square_error_test_linear():
     
    err_sum = 0
 
    for index in range(len(test_x)):
        y_pred = weights[0]
        for i in range(13):
            y_pred += weights[i+1]*test_x[index][i]
         
        err_sum += (test_y[index] - y_pred)**2
    
    print
    print 'Mean Squared Error - Test:',(err_sum / len(test_x))
    

'''
    lambda = 0.01; 0.1; 1.0,
'''
def get_ridge_weight(lam):
    
    iden = numpy.zeros((14,14), float)
    
    
    numpy.fill_diagonal(iden, lam )

    x_bar = [[1]+ i for i in train_x]
 
    x = numpy.array(x_bar)
    x_t = matrix.transpose(x)
     
    x_x_t = numpy.dot(x_t, x)
    
    x_x_t_i = numpy.add(x_x_t, iden)
    y = numpy.array(train_y)
    x_y_t = numpy.dot(x_t, y)
      
    w = numpy.linalg.solve(x_x_t_i, x_y_t)
      
    return w

def get_mean_square_error_train_ridge():
     
    err_sum = 0
 
    for index in range(len(train_x)):
        y_pred = r_weights[0]
        for i in range(13):
            y_pred += r_weights[i+1]*train_x[index][i]
         
        err_sum += (train_y[index] - y_pred)**2
    
    print 'Mean Squared Error - Training:',(err_sum / len(train_x))
    
def get_mean_square_error_test_ridge():
     
    err_sum = 0
 
    for index in range(len(test_x)):
        y_pred = r_weights[0]
        for i in range(13):
            y_pred += r_weights[i+1]*test_x[index][i]
         
        err_sum += (test_y[index] - y_pred)**2
    
    print 'Mean Squared Error - Test:',(err_sum / len(test_x))
    
    
def get_sample_weight(data_x, data_y, ignore, lam):
    
    x = numpy.delete(data_x, ignore, 0)
    y = numpy.delete(data_y, ignore, 0)
    
    x_bar = []
    y_bar = []
    
    for vals in x:
        for inner_vals in vals:
            x_bar.append([1]+ inner_vals)
            
    for vals in y:
        for inner_vals in vals:
            y_bar.append(inner_vals)
    
    x_bar = numpy.array(x_bar)
    y_bar = numpy.array(y_bar)
    
    iden = numpy.zeros((14,14), float)
    numpy.fill_diagonal(iden, lam )    
    
    x_transpose = matrix.transpose(x_bar)
    x_and_x_transpose = numpy.dot(x_transpose, x_bar)
    x_and_x_transpose_iden = numpy.add(x_and_x_transpose, iden)
    x_transpose_and_y = numpy.dot(x_transpose, y_bar)
    
    w = numpy.linalg.solve(x_and_x_transpose_iden, x_transpose_and_y)
    
    return w
    

def get_10fold_cv_data():
    
    cv_data = copy.deepcopy(train_x)
    
    for i in range(len(train_y)):
        cv_data[i].append(train_y[i])
        
    
    ran.shuffle(cv_data)
     
    sample_x = [[] for _ in range(10)]
    sample_y = [[] for _ in range(10)]
    for i in range(7):
        sample_x[i] = [x[0: 13]  for x in cv_data[i*43 : (i+1)*43] ]
        sample_y[i] = [x[13]  for x in cv_data[i*43 : (i+1)*43] ]
        
    
    start = 43*7
    for i in range(3):
        sample_x[7 + i] = [x[0: 13]  for x in cv_data[start + i*44 : start + (i+1)*44] ]
        sample_y[7 + i] = [x[13]  for x in cv_data[start + i*44 : start + (i+1)*44] ]
    
    return sample_x, sample_y

def process_CV():
    
    sample_x, sample_y = get_10fold_cv_data()
    
    start = 0.0
    
    lambdas = []
    for _ in range(10000):
        start+=0.01
        lambdas.append(round(start, 2))
        if start > 10:
            break
    
  
    
#     print 'Lambda Values :', lambdas
    
    min_lambda_val = None
    min_val = float('inf')
    
    for l in lambdas:
        
#         print
#         print 'Processing Lambda: ',l
        
        err_sum_arr = []
        avg = 0
        for i in range(10):
            
            sample_w = get_sample_weight(sample_x, sample_y, i, l)
#             print 'w: ', sample_w
            
            testx = sample_x[i]
            texty = sample_y[i]
            
            err_sum = 0
     
            for index in range(len(testx)):
                y_pred = sample_w[0]
                for i in range(13):
                    y_pred += sample_w[i+1]*testx[index][i]
                 
                err_sum += (texty[index] - y_pred)**2
                
            err_sum_arr.append(round(err_sum / len(testx),3))    
            
            avg += round((err_sum / len(testx)),3)
        
#         print 'MSE - Array'
#         print err_sum_arr
        avg = round(avg / 10, 3)
#         print 'Average of CV: ', avg
       
        if min_val > avg:
            min_val = avg
            min_lambda_val = l
    
    print
    print 'Best Selected Lambda and its avg MSE Train - Value: ',min_lambda_val, '-' , min_val
    
    return min_lambda_val
            

def get_linear_weights_for_4correlated_Features(data_train):
    
    x_bar = [[1]+ i for i in data_train]
    x = numpy.array(x_bar)
    x_t = matrix.transpose(x)
    
    x_x_t = numpy.dot(x_t, x)
    y = numpy.array(train_y)
    x_y_t = numpy.dot(x_t, y)
    
    w = numpy.linalg.solve(x_x_t, x_y_t)
    
    return w

        

def get_mean_square_error_train_for_4correlated_Features(data_train, rg):
#      four_correlated_feature_data_train
#      weights_4correlated_data
    err_sum = 0
 
    for index in range(len(data_train)):
        y_pred = weights_4correlated_data[0]
        for i in range(rg):
            y_pred += weights_4correlated_data[i+1]*data_train[index][i]
         
        err_sum += (train_y[index] - y_pred)**2
    
    return (err_sum / len(data_train))
    
def get_mean_square_error_test_for_4correlated_Features(data_test, rg):
#      four_correlated_feature_data_test
    err_sum = 0
 
    for index in range(len(data_test)):
        y_pred = weights_4correlated_data[0]
        for i in range(rg):
            y_pred += weights_4correlated_data[i+1]*data_test[index][i]
         
        err_sum += (test_y[index] - y_pred)**2 
    
    return (err_sum / len(data_test))
    

def get_linear_weights_for_4correlated_Features_pb(data_train_x, data_train_y):
    
    x_bar = [[1]+ i for i in data_train_x]
    x = numpy.array(x_bar)
    x_t = matrix.transpose(x)
    
    x_x_t = numpy.dot(x_t, x)
    y = numpy.array(data_train_y)
    x_y_t = numpy.dot(x_t, y)
    
    w = numpy.linalg.solve(x_x_t, x_y_t)
    
    return w

def get_residue_array(data_x, data_y, wg, rg):
    
    res = []
    
    for index in range(len(data_x)):
        y_pred = wg[0]
        for i in range(rg):
            y_pred += wg[i+1] * data_x[index][i]
            
        res.append(data_y[index] - y_pred)
            
    return res   

def get_pearson_coefficients_pb(data_y):
    p_values = []
    
    for i in range(len(attr_array)):
        p_values.append(round(pearson(attr_array[i], data_y)[0],2))
    
#     print 'Pearson Coefficents - Pb:',p_values
    return p_values

    

# def get_min(parray, larray):
#     
#     max = -1
#     print parray
#     max_label = None
#     for i in range(len(parray)):
#         if(max < abs(parray[i])):
#             max = i
#             max_label = larray[i]
#     
#     return max_label 
    
    
    
#----------------------------------------------Start Here -----------------------------------------####    
    
boston = load_boston()

test_x = []
train_x = []
test_y = []
train_y = []


for i in range(0, len(boston.data)):
    if(i % 7 == 0):
        test_x.append(list(boston.data[i]))
        test_y.append(boston.target[i])
    else:
        train_x.append(list(boston.data[i]))
        train_y.append(boston.target[i])
        
        
attr_array = [[] for _ in range(13)]
 
for key in train_x:
    for i in range(0, 13):
        attr_array[i].append(key[i])

hist_array = copy.deepcopy(attr_array)


get_pearson_coefficients()
mean_arr, std_arr = get_mean_median()
normalize_train_data()
weights = get_linear_weights()

print'------------------------------Linear Regression-----------------------------'
get_mean_square_error_train_linear()
normalize_test_data()
get_mean_square_error_test_linear()


'''
    Ridge Regression
'''
print
print '------------------------------Ridge Regression-----------------------------'
print
l = [0.01, 0.1, 1.0]

for lam in l:
    print "Lambda :",lam
    r_weights = get_ridge_weight(lam)
    get_mean_square_error_train_ridge()
    get_mean_square_error_test_ridge()


'''
Divide data in 
'''
# print
# print '------------------------------Cross Validation-----------------------------'
# 
# l_a = []
# for i in range(10):
# l_a.append(process_CV())

# l_a = [10.0]

# for lam in l_a:
#     print "Lambda :",lam
#     r_weights = get_ridge_weight(lam)
# #     get_mean_square_error_train_ridge()
#     get_mean_square_error_test_ridge()
 


#2,5,10,12
print
print '------------------------------Feature Selection-----------------------------' 
print
print '------------------------------Selection with correlation ---------------------'
print
print '------------------------------4 highest correlated data - Part a---------------------'  

features = [2,5,10,12]
four_correlated_feature_data_train = []
for arr in train_x:
    four_correlated_feature_data_train.append([arr[i] for i in features])


four_correlated_feature_data_test = []

for arr in test_x:
    four_correlated_feature_data_test.append([arr[i] for i in features])
    
weights_4correlated_data = get_linear_weights_for_4correlated_Features(four_correlated_feature_data_train)

print
print 'Four Highest correlated Features with target are:',features
print
print 'Mean Squared Error - Training:',get_mean_square_error_train_for_4correlated_Features(four_correlated_feature_data_train, 4)
print
print 'Mean Squared Error - Test:',get_mean_square_error_test_for_4correlated_Features(four_correlated_feature_data_test, 4)
print
print '------------------------------4 highest correlated data - Part b ---------------------'

features = [12]
partbdata_train_x = []
partbdata_test_x =[]

partbdata_train_y = train_y
partbdata_test_y = test_y

for arr in train_x:
    partbdata_train_x.append([arr[i] for i in features])

for arr in test_x:
    partbdata_test_x.append([arr[i] for i in features])

wghts = get_linear_weights_for_4correlated_Features_pb(partbdata_train_x, partbdata_train_y)

residue = get_residue_array(partbdata_train_x, partbdata_train_y, wghts, 1)
attr_array = [[] for _ in range(12)] 

indarr = []
for i in range(13):
    if i not in features:
        indarr.append(i)
 
for key in train_x:
    for i in range(13):
        if i not in features:
            attr_array[i].append(key[i])
 
# print attr_array[0]
pv = get_pearson_coefficients_pb(residue)
# print pv
# print get_min(pv, indarr)



    
 
 
features.append(5)
 
partbdata_train_y = residue
partbdata_train_x = []
 
for arr in train_x:
    partbdata_train_x.append([arr[i] for i in features])
  
wghts = get_linear_weights_for_4correlated_Features_pb(partbdata_train_x, partbdata_train_y)
 
residue = get_residue_array(partbdata_train_x, partbdata_train_y, wghts, 2)
 
 
 
indarr = []
for i in range(13):
    if i not in features:
        indarr.append(i)
  
# print indarr
  
  
att_r = []
for arr in train_x:
    att_r.append([arr[i] for i in indarr])
  
attr_array = matrix.transpose(numpy.array(att_r))
 
pv = get_pearson_coefficients_pb(residue)
# print pv
# print get_min(pv, indarr)


features.append(10)
 
partbdata_train_y = residue
partbdata_train_x = []
 
for arr in train_x:
    partbdata_train_x.append([arr[i] for i in features])
  
wghts = get_linear_weights_for_4correlated_Features_pb(partbdata_train_x, partbdata_train_y)
 
residue = get_residue_array(partbdata_train_x, partbdata_train_y, wghts, 3)
 
 
 
indarr = []
for i in range(13):
    if i not in features:
        indarr.append(i)
  
# print indarr
  
  
att_r = []
for arr in train_x:
    att_r.append([arr[i] for i in indarr])
  
attr_array = matrix.transpose(numpy.array(att_r))
 
pv = get_pearson_coefficients_pb(residue)
# print pv
# print get_min(pv, indarr)

features.append(3)
 
partbdata_train_y = residue
partbdata_train_x = []
 
for arr in train_x:
    partbdata_train_x.append([arr[i] for i in features])
  
wghts = get_linear_weights_for_4correlated_Features_pb(partbdata_train_x, partbdata_train_y)
 
residue = get_residue_array(partbdata_train_x, partbdata_train_y, wghts, 4)
 
 
 
indarr = []
for i in range(13):
    if i not in features:
        indarr.append(i)
  
# print indarr
  
  
att_r = []
for arr in train_x:
    att_r.append([arr[i] for i in indarr])
  
attr_array = matrix.transpose(numpy.array(att_r))
 
# 
print
print "Select Features based on residues are: ", features

four_correlated_feature_data_train = []
for arr in train_x:
    four_correlated_feature_data_train.append([arr[i] for i in features])


four_correlated_feature_data_test = []

for arr in test_x:
    four_correlated_feature_data_test.append([arr[i] for i in features])
    
weights_4correlated_data = get_linear_weights_for_4correlated_Features(four_correlated_feature_data_train)

print
print 'Mean Squared Error - Training:',get_mean_square_error_train_for_4correlated_Features(four_correlated_feature_data_train, 4)
print
print 'Mean Squared Error - Test:',get_mean_square_error_test_for_4correlated_Features(four_correlated_feature_data_test, 4)

print 
print '------------------------------Selection based on brute force ---------------------'

l = range(13)

combinations = itertools.combinations(l, 4)

min = float('inf')
min_test = None
min_features = None

for combo in combinations:
    features = list(combo)
    four_correlated_feature_data_train = []
    
    for arr in train_x:
        four_correlated_feature_data_train.append([arr[i] for i in features])
    
        
    weights_4correlated_data = get_linear_weights_for_4correlated_Features(four_correlated_feature_data_train)
    
    val = get_mean_square_error_train_for_4correlated_Features(four_correlated_feature_data_train, 4)
    
    if min > val:
        min = val
        min_features = features
    
print
print "Select Features based on Brute Force are: ", min_features
features = min_features

four_correlated_feature_data_train = []
for arr in train_x:
    four_correlated_feature_data_train.append([arr[i] for i in features])


four_correlated_feature_data_test = []

for arr in test_x:
    four_correlated_feature_data_test.append([arr[i] for i in features])
    
weights_4correlated_data = get_linear_weights_for_4correlated_Features(four_correlated_feature_data_train)

print
print 'Mean Squared Error - Training:',get_mean_square_error_train_for_4correlated_Features(four_correlated_feature_data_train, 4)
print
print 'Mean Squared Error - Test:',get_mean_square_error_test_for_4correlated_Features(four_correlated_feature_data_test, 4)

print
print '------------------------------Polynomial Feature Expansion ---------------------'

poly_feature_data_train = []
poly_feature_data_test = []

for i in range(len(train_x)):
    arr = []
    for j in range(len(train_x[i])):
        for k in range(j, len(train_x[i])):
            arr.append(train_x[i][j] * train_x[i][k])
    poly_feature_data_train.append(arr)


for i in range(len(test_x)):
    arr = []
    for j in range(len(test_x[i])):
        for k in range(j, len(test_x[i])):
            arr.append(test_x[i][j] * test_x[i][k])
    poly_feature_data_test.append(arr)


# print poly_feature_data_train[0]

mean_arr = numpy.mean(poly_feature_data_train, axis = 0)
std_arr = numpy.std(poly_feature_data_train, axis=0, ddof = 1)

for values in poly_feature_data_train:
        for i in range(len(values)):
            values[i] = (values[i] - mean_arr[i]) / std_arr[i]
    

for values in poly_feature_data_test:
        for i in range(len(values)):
            values[i] = (values[i] - mean_arr[i]) / std_arr[i]

poly_feature_data_train = [train_x[i]+poly_feature_data_train[i] for i in range(len(poly_feature_data_train))]
poly_feature_data_test = [test_x[i]+poly_feature_data_test[i] for i in range(len(poly_feature_data_test))]




weights_4correlated_data = get_linear_weights_for_4correlated_Features_pb(poly_feature_data_train, train_y)

print
print 'Mean Squared Error - Training:', get_mean_square_error_train_for_4correlated_Features(poly_feature_data_train, 104)
print
print 'Mean Squared Error - Test:', get_mean_square_error_test_for_4correlated_Features(poly_feature_data_test, 104)

attributes = ['CRIM','ZN','INDUS','CHAS','NOX','RM','AGE','DIS','RAD','TAX','PTRATIO','B','LSTAT']

fig =pt.figure()
fig.subplots_adjust(left=0.125, bottom=0.1, right=0.9, top=0.9, wspace=0.4, hspace=0.4)
# pt.title("Histogram for all features")
for i in range(13):

    fig.add_subplot(5, 3, i+1).hist(hist_array[i])
#     fig.add_subplot(5, 3, i+1).set_xlabel('Values')
    fig.add_subplot(5, 3, i+1).set_ylabel('Frequency')
    fig.add_subplot(5, 3, i+1).set_title(attributes[i])
    
pt.show()



