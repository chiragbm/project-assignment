'''
Created on Jun 18, 2016

@author: DELL
'''
import sys
import re

def unify(a, b, sub):
    
    if sub == False:
        return False
    elif a == b:
        return sub
    elif a == 'x':
        return unify_var(a, b, sub)
    elif b == 'x':
        return unify_var(b, a, sub)
    elif is_compound(a) and is_compound(b):
        return unify(convert_args_list(a), convert_args_list(b), unify(get_predicate_string(a), get_predicate_string(b), sub))
    elif isinstance(a, list) and isinstance(b, list):
        return unify(rest(a), rest(b), unify(first(a), first(b), sub))
    else: 
        return False
    
def is_compound(var):
    #Check does it has ( and ) curly braces:
    if not (isinstance(var, str) and var.find("(") != -1 and var.find(")") != -1):
        return False
    else:
        return True
    
def convert_args_list(var):
    
    l = list()
    new = var[var.find("(")+1: var.rfind(")")]
    if new.find(",") !=-1:
        arg1 = new[0: new.find(",")]
        arg2 = new[new.find(",")+1 : len(new)]
        l.append(arg1.strip())
        l.append(arg2.strip())
    else:
        arg1 = new[0:]
        l.append(arg1.strip())
    
    return l
        
def get_predicate_string(var):
    pred = re.match(r'\w*', var)
    return pred.group()

def first(var):
    item = var[0]
    return item

def rest(var):
    items = var[1:]
    return items

def unify_var(var, x, sub):
    
    if sub.__contains__(var):
        return unify(sub[var], x, sub)
    elif sub.__contains__(x):
        return unify(var, sub[x], sub)
    elif occurs_check(var, x):
        return False
    else:
        sub[var] = x
        return sub

def occurs_check(var, x):
    
    if is_compound(x):
        items = convert_args_list(x)
        if var in items:
            return True
    
    return False


def bc_ask(query, sub):
    # Check is query a key    
    if implications.__contains__(query):
        # constructs is a list contains all values for this key
        constructs = implications[query]
        if len(constructs) <= 1:
            write_query_fact(constructs[0], query)
            c_list = get_list(constructs[0])
            i = 0;
            for item in c_list:
                write_evaluation_query(item)
                output = list()
                output = bc_ask(item, output)
                write_query_value(item, output)
                if not (i ==0):
                    sub = intersection(sub, output)
                else:
                    sub = output
                    i+=1
        else:
            for predicate in constructs:
                write_query_fact(predicate, query)
                result = list()
                c_list = get_list(predicate)
                i =0;
                for item in c_list:
                    write_evaluation_query(item)
                    output = list()
                    output = bc_ask(item, output)
                    write_query_value(item, output)
                    if not (i ==0):
                        result = intersection(result, output)
                    else:
                        result = output
                    i+=1
                    
                sub = union(sub, result)
                                              

    for fact in facts:
        theta = {}
        result = unify(query, fact, theta)
        if (result != False) and result.__contains__('x'):
            if not result['x'] in sub:
                sub.append(result['x'])
    
    return sub


def intersection(a_list, b_list):
    combo_list = list(set(a_list) & set(b_list))
    return combo_list

def union(a_list, b_list):
    combo_list = list(set(a_list) | set(b_list))
    return combo_list


def get_list(constructs):
    l = re.split(r'&', constructs)
    return l


def bc_ask_var(query):
    
    if query in facts:
        return True
    
    output = False
    for key in implications.keys():
        theta ={}
        #unify each key with the query
        result = unify(query, key, theta)
        if (result != False) and result.__contains__('x'):
            # get constructs forming query - returns list
            constructs = implications[key]
            ans = True
            for fact in constructs:
            # returns facts list that implies this key
                write_query_fact(fact, key)
                c_list = get_list(fact)
                for item in c_list:
                    write_evaluation_query(item)
                    q = replace_var_in_query(item, result['x'])
                    c_value = bc_ask_var(q)
                    write_query_value(item, c_value)
                    ans = ans and c_value
                output = output or ans
    return output

def replace_var_in_query(query, value):
    args = convert_args_list(query)
    var = 'x'
    if var in args:
        loc = args.index(var)
        args[loc] = value
    
    out = []
    out.append(get_predicate_string(query))
    out.append('(')
    
    toggle = 1
    for i in args:
        if toggle!=1:
            out.append(',')
        else:
            toggle = 2
            
        out.append(i)
    out.append(')')
    
    s = "".join(out)
    
    return s


def backward_chain_for_var(query):

    write_evaluation_query(query)
    theta = []
    ques_list = get_list(query)
    final_output = []
    if len(ques_list) > 1:
        i = 0
        for que in ques_list:
            write_evaluation_query(que)
            ans = bc_ask(que, theta)
            write_query_value(que, ans)
            if not (i ==0):
                final_output = intersection(final_output, ans)
            else:
                final_output = ans
                i+=1
    else:
        final_output = bc_ask(query, theta)
    write_query_value(query, final_output)
    

def backward_chain_for_value(query):
    
    write_evaluation_query(query)
    ques_list = get_list(query)
    final_output = True
    if len(ques_list)> 1:    
        for que in ques_list:
            write_evaluation_query(que)
            ans = bc_ask_var(que)
            write_query_value(que, ans)
            final_output = final_output and ans
    else:
        final_output = bc_ask_var(query)
    write_query_value(query, final_output)
    
              
def write_query_fact(predicate, fact):
    o.write("Query: %s=>%s\n"%(predicate,fact)) 

def write_evaluation_query(query): 
    o.write("Query: %s\n"%(query))

def write_query_value(query, value):
    if isinstance(value, list):
        if len(value) > 0:
            o.write('%s: True: %s\n'%(query, sorted(value)))
        else:
            o.write('%s: False\n'%(query))
    else:
        o.write("%s: %s\n"%(query,value))
    


'''
    Read Input File
'''    
inp = open(sys.argv[1], "r")
o = open("output.txt",'w+')
ask = (inp.readline()).strip()
clauses_len = int(inp.readline())


'''
    Preparing knowledge base
'''
implications = {}
facts = [] 
for i in range(clauses_len):
    line = inp.readline()
    line = line.strip()
    length = (len(line))
    index = line.find('=>')
    if not index == -1:
        ## Add to implication Dict
        
        key = line[index+2:length]
        value = line[0:index]
        
        if (implications.__contains__(key)) :
            lst = implications[key]
            lst.append(value)
        else:
            lst = list()
            lst.append(value)
            implications[key] = lst
    else:
        ## Add to Atomic list
        facts.append(line)       

'''
    Reading Question:
        - Identify whether it has variable 'x' or not
'''
ques_var = 'x'
ques_contains_var = False

questions = get_list(ask)
for question in questions:
    arguments = convert_args_list(question)
    if ques_var in arguments:
        ques_contains_var = True
        break

if ques_contains_var:
    backward_chain_for_var(ask)
else:
    backward_chain_for_value(ask)

'''
    Close files
'''
o.close();
inp.close()

        
