players = { 1:"b",
           2:"w"
           }


def checkWin(gmBoard, i, j, ply, N):
    win = False
    
    # Check Diagonals
    if not win :
        cont = 0
        for a in range(-4,5):
            
            if ( i+a >=0 and i+a < N) and (j+a >=0 and j+a < N):
                if(a==0):
                    cont+=1
                elif gmBoard[i+a][j+a] == players[ply]:
                    cont+=1
                else:
                    cont = 0                     
            else:
                cont = 0
                
            if cont==5:
                win = True
                break
    
    # Check Diagonals
    if not win :
        cont = 0
        for a in range(-4,5):
            
            if ( i+a >=0 and i+a < N) and (j-a >=0 and j-a < N):
                if(a==0):
                    cont+=1
                elif gmBoard[i+a][j-a] == players[ply]:
                    cont+=1
                else:
                    cont = 0                     
            else:
                cont = 0
                
            if cont==5:
                win = True
                break
    
    #Check Horizontal
    
    if not win :
        cont = 0
        for a in range(-4,5):
            
            if  i-a >=0 and i-a < N:
                if(a==0):
                    cont+=1
                elif gmBoard[i-a][j] == players[ply]:
                    cont+=1
                else:
                    cont = 0                     
            else:
                cont = 0
                
            if cont==5:
                win = True
                break
    
    #Check Vertical
    if not win :
        cont = 0
        for a in range(-4,5):
            
            if j-a >=0 and j-a < N:
                if(a==0):
                    cont+=1
                elif gmBoard[i][j-a] == players[ply]:
                    cont+=1
                else:
                    cont = 0                     
            else:
                cont = 0
                
            if cont==5:
                win = True
                break   
    
    return win

def blockClosedFour(gmBoard, i, j, ply, N):
    block = 0
    
    go = False
    if i+5 < N and j+5 < N:
        if gmBoard[i+5][j+5]== players[ply] :
            go = True
     
    elif (i+5 ==N and j+5 <=N) or (i+5 <=N and j+5==N):
        go = True
          
    if go:
        p = 3-ply
        for a in range(1,5):
            if not (gmBoard[i+a][j+a] == players[p]):
                break
        else:
            block+=1
     

    go = False
    if i-5 >=0 and j-5>=0:
        if gmBoard[i-5][j-5]== players[ply]:
            go = True
             
    elif (i-5 == -1 or j-5==-1):
        go = True
         
    if go :
        p = 3-ply
        for a in range(1,5):
            if not (gmBoard[i-a][j-a] == players[p]):
                break
        else:
            block+=1
            
             
    go = False
    if i-5 >=0 :
        if gmBoard[i-5][j]== players[ply]:
            go = True
     
    elif (i-5 == -1 ):
        go = True
         
    if go :
        p = 3-ply
        for a in range(1,5):
            if not (gmBoard[i-a][j] == players[p]):
                break
        else:
            block+=1
 
    go = False
    if j+5 < N :
        if gmBoard[i][j+5]== players[ply]:
            go = True
    elif j+5 == N :
        go = True
         
    if go:
        p = 3-ply
        for a in range(1,5):
            if not (gmBoard[i][j+a] == players[p]):
                break
        else:
            block+=1
 
    go = False
    if i+5 < N :
        if gmBoard[i+5][j]== players[ply]:
            go = True
    elif i+5 == N :
        go = True
         
    if go:
        p = 3-ply
        for a in range(1,5):
            if not (gmBoard[i+a][j] == players[p]):
                break
        else:
            block+=1
 
    go = False
    if j-5 >=0 :
        if gmBoard[i][j-5]== players[ply]:
            go = True
         
    elif j-5 == -1 :
        go = True
         
    if go :
        p = 3-ply
        for a in range(1,5):
            if not (gmBoard[i][j-a] == players[p]):
                break
        else:
            block+=1

    go = False
    if i+5 < N and j-5 >= 0 :
        if gmBoard[i+5][j-5]== players[ply]:
            go = True
    elif (i+5 == N and j-5 >= -1 ) or (i+5 <= N and j-5 == -1):
        go = True
        
    if go:
        p = 3-ply
        for a in range(1,5):
            if not (gmBoard[i+a][j-a] == players[p]):
                break
        else:
            block+=1

    go = False
    if i-5 >= 0 and j+5 < N :
        if gmBoard[i-5][j+5]== players[ply]:
            go = True
    elif (i-5 == -1 and j+5 <= N) or (i-5 >= -1 and j+5 == N) :
        go = True
         
    if go:
        p = 3-ply
        for a in range(1,5):
            if not (gmBoard[i-a][j+a] == players[p]):
                    break
        else:
            block+=1
    
    return block

def createClosedFour(gmBoard, i, j, ply, N):
    close = 0
    
    #Check Diagonals
    count = 0;
    for a in range(-5,6):
        if ( i+a >=0 and i+a < N) and (j+a >=0 and j+a < N):
            if a == 0 or (gmBoard[i+a][j+a] == players[ply]):
                if count==4:
                    count = 0
                else:
                    count+=1
            else:
                if count ==4:
                    if gmBoard[i+a][j+a] == players[3-ply]:
                        if i+a-5 >=0 and j+a-5 >=0:
                            if gmBoard[i+a-5][j+a-5] == '.':
                                close+=1
                                break
                            else:
                                count = 0
                        else:
                            count = 0
                    elif gmBoard[i+a][j+a] == '.':
                        if i+a-5 >=0 and j+a-5 >=0:
                            if gmBoard[i+a-5][j+a-5] == players[3-ply]:
                                close+=1
                                break
                            else:
                                count = 0
                        elif i+a-5 == -1 or j+a-5 ==-1:
                            close+=1
                            break
                        else:
                            count = 0
                        
                else:
                    count =0
        elif i+a == N or j+a ==N:
            if count ==4 and gmBoard[i+a-5][j+a-5] == '.':
                if i+a-5 >=0 and j+a-5 >=0:
                    close+=1
                    break
                

#Check Diagonals
    count = 0;
    for a in range(-5,6):
        if ( i+a >=0 and i+a < N) and (j-a >=0 and j-a < N):
            if a == 0 or (gmBoard[i+a][j-a] == players[ply]):
                if count==4:
                    count = 0
                else:
                    count+=1
            else:
                if count ==4:
                    if gmBoard[i+a][j-a] == players[3-ply]:
                        if i+a-5 >=0 and j-a+5 >=0:
                            if gmBoard[i+a-5][j-a+5] == '.':
                                close+=1
                                break
                            else:
                                count = 0
                        else:
                            count = 0
                    elif gmBoard[i+a][j-a] == '.':
                        if i+a-5 >=0 and j-a+5 < N:
                            if gmBoard[i+a-5][j-a+5] == players[3-ply]:
                                close+=1
                                break
                            else:
                                count = 0
                        elif i+a-5 == -1 or j-a+5 ==N:
                            close+=1
                            break
                        else:
                            count = 0
                        
                else:
                    count =0
        elif i+a == N or j-a ==-1:
            #print(i+a, j+a)
            if count ==4 and gmBoard[i+a-5][j-a+5] == '.':
                if i+a-5 >= 0 and j-a+5 < N:
                    close+=1
                    break
                

#Check for horizontal 
    count = 0;
    for a in range(-5,6):
        if (j+a >=0 and j+a < N):
            if a == 0 or (gmBoard[i][j+a] == players[ply]):
                if count==4:
                    count = 0
                else:
                    count+=1
            else:
                if count ==4:
                    if gmBoard[i][j+a] == players[3-ply]:
                        if j+a-5 >=0:
                            if gmBoard[i][j+a-5] == '.':
                                close+=1
                                break
                            else:
                                count = 0
                        else:
                            count = 0
                    elif gmBoard[i][j+a] == '.':
                        if j+a-5 >=0:
                            if gmBoard[i][j+a-5] == players[3-ply]:
                                close+=1
                                break
                            else:
                                count = 0
                        elif j+a-5 ==-1:
                            close+=1
                            break
                        else:
                            count = 0
                        
                else:
                    count =0
        elif j+a ==N:
            if count ==4 and gmBoard[i][j+a-5] == '.':
                if j+a-5 >=0:
                    close+=1
                    break

#Check for vertical
    count = 0;
    for a in range(-5,6):
        if ( i+a >=0 and i+a < N) :
            if a == 0 or (gmBoard[i+a][j] == players[ply]):
                if count==4:
                    count = 0
                else:
                    count+=1
            else:
                if count ==4:
                    if gmBoard[i+a][j] == players[3-ply]:
                        if i+a-5 >=0 :
                            if gmBoard[i+a-5][j] == '.':
                                close+=1
                                break
                            else:
                                count = 0
                        else:
                            count = 0
                    elif gmBoard[i+a][j] == '.':
                        if i+a-5 >=0:
                            if gmBoard[i+a-5][j] == players[3-ply]:
                                close+=1
                                break
                            else:
                                count = 0
                        elif i+a-5 == -1 :
                            close+=1
                            break
                        else:
                            count = 0
                        
                else:
                    count =0
        elif i+a == N :
            if count ==4 and gmBoard[i+a-5][j] == '.':
                if i+a-5 >=0:
                    close+=1
                    break
                
                
    return close

def blockClosedThree(gmBoard, i, j, ply, N):
    block = 0
    
    #Check for diagonals / upside
    go = False
    if i+4 < N and j+4 < N:
        if gmBoard[i+4][j+4]== players[ply] :
            go = True
     
    elif(i+4 ==N and j+4 <=N) or (i+4 <=N and j+4==N):
        go = True
          
    if go:
        p = 3-ply
        for a in range(1,4):
            if not gmBoard[i+a][j+a] == players[p]:
                break
        else:
            block +=1
            
#Check for diagonals / down side
    go = False
    if i-4 >=0 and j-4>=0:
        if gmBoard[i-4][j-4]== players[ply]:
            go = True
             
    elif (i-4 == -1 and j-4>=-1) or (i-4 >= -1 and j-4==-1):
        go = True
         
    if go :
        p = 3-ply
        for a in range(1,4):
            if not (gmBoard[i-a][j-a] == players[p]):
                break
        else:
            block +=1



#Check for down side - vertical
    go = False
    if i-4 >=0 :
        if gmBoard[i-4][j]== players[ply]:
            go = True
     
    elif (i-4 == -1 ):
        go = True
         
    if go :
        p = 3-ply
        for a in range(1,4):
            if not (gmBoard[i-a][j] == players[p]):
                break
        else:
            block +=1


#Check for horizontal - right side
    go = False
    if j+4 < N :
        if gmBoard[i][j+4]== players[ply]:
            go = True
    elif j+4 == N :
        go = True
         
    if go:
        p = 3-ply
        for a in range(1,4):
            if not (gmBoard[i][j+a] == players[p]):
                break
        else:
            block +=1


#Check for vertical - upside
    go = False
    if i+4 < N :
        if gmBoard[i+4][j]== players[ply]:
            go = True
    elif i+4 == N :
        go = True
         
    if go:
        p = 3-ply
        for a in range(1,4):
            if not (gmBoard[i+a][j] == players[p]):
                break
        else:
            block +=1

#Check for horizontal left side
    go = False
    if j-4 >=0 :
        if gmBoard[i][j-4]== players[ply]:
            go = True
         
    elif j-4 == -1 :
        go = True
         
    if go :
        p = 3-ply
        for a in range(1,4):
            if not (gmBoard[i][j-a] == players[p]):
                break
        else:
            block +=1
            
    go = False
    if i+4 < N and j-4 >= 0 :
        if gmBoard[i+4][j-4]== players[ply]:
            go = True
    elif (i+4 == N and j-4 >= -1 ) or (i+4 <= N and j-4 == -1):
        go = True
        
    if go:
        p = 3-ply
        for a in range(1,4):
            if not (gmBoard[i+a][j-a] == players[p]):
                break
        else:
            block +=1


    go = False
    if i-4 >= 0 and j+4 < N :
        if gmBoard[i-4][j+4]== players[ply]:
            go = True
    elif (i-4 == -1 and j+4 <= N) or (i-4 >= -1 and j+4 == N) :
        go = True
         
    if go:
        p = 3-ply
        for a in range(1,4):
            if not (gmBoard[i-a][j+a] == players[p]):
                    break
        else:
            block +=1
            
            
    return block

def blockOpenThree(gmBoard, i, j, ply, N):
    
    block = 0
    
    #Check for diagonals / upside
    go = False
    if i+4 < N and j+4 < N:
        if gmBoard[i+4][j+4]== "." :
            go = True
          
    if go:
        p = 3-ply
        for a in range(1,4):
            if not gmBoard[i+a][j+a] == players[p]:
                break
        else:
            block+=1
            
    #Check for diagonals / upside
    go = False
    if i-4 >= N and j-4 >= N:
        if gmBoard[i-4][j-4]== "." :
            go = True
          
    if go:
        p = 3-ply
        for a in range(1,4):
            if not gmBoard[i-a][j-a] == players[p]:
                break
        else:
            block+=1
    
    go = False
    if j-4 >= 0 :
        if gmBoard[i][j-4]== ".":
            go = True
         
    if go:
        p = 3-ply
        for a in range(1,4):
            if not (gmBoard[i][j-a] == players[p]):
                break
        else:
            block+=1
        
            
    #Check for horizontal - right side
    go = False
    if j+4 < N :
        if gmBoard[i][j+4]== ".":
            go = True
         
    if go:
        p = 3-ply
        for a in range(1,4):
            if not (gmBoard[i][j+a] == players[p]):
                break
        else:
            block+=1

    go = False
    if i-4 >= 0 :
        if gmBoard[i-4][j]== ".":
            go = True
          
    if go:
        p = 3-ply
        for a in range(1,4):
            if not (gmBoard[i-a][j] == players[p]):
                break
        else:
            block+=1
    
    go = False
    if i+4 < N :
        if gmBoard[i+4][j]== ".":
            go = True
          
    if go:
        p = 3-ply
        for a in range(1,4):
            if not (gmBoard[i+a][j] == players[p]):
                break
        else:
            block+=1

    go = False
    if i+4 < N and j-4 >=0 :
        if gmBoard[i+4][j-4]== ".":
            go = True
         
    if go:
        p = 3-ply
        for a in range(1,4):
            if not (gmBoard[i+a][j-a] == players[p]):
                    break
        else:
            block+=1
    
    
    go = False
    if i-4 >= 0 and j+4 < N :
        if gmBoard[i-4][j+4]== ".":
            go = True
         
    if go:
        p = 3-ply
        for a in range(1,4):
            if not (gmBoard[i-a][j+a] == players[p]):
                    break
        else:
            block+=1
                
    return block
                    
def createClosedThree(gmBoard, i, j, ply, N):
    
    close = 0
    
    #Check Diagonals
    count = 0;
    for a in range(-4,5):
        if ( i+a >=0 and i+a < N) and (j+a >=0 and j+a < N):
            if a == 0 or (gmBoard[i+a][j+a] == players[ply]):
                if count==3:
                    count = 0
                else:
                    count+=1
            else:
                if count ==3:
                    if gmBoard[i+a][j+a] == players[3-ply]:
                        if i+a-4 >=0 and j+a-4 >=0:
                            if gmBoard[i+a-4][j+a-4] == '.':
                                close+=1
                                break
                            else:
                                count = 0
                        else:
                            count = 0
                    elif gmBoard[i+a][j+a] == '.':
                        if i+a-4 >=0 and j+a-4 >=0:
                            if gmBoard[i+a-4][j+a-4] == players[3-ply]:
                                close+=1
                                break
                            else:
                                count = 0
                        elif i+a-4 == -1 or j+a-4 ==-1:
                            close+=1
                            break
                        else:
                            count = 0
                        
                else:
                    count =0
        elif i+a == N or j+a ==N:
            if count ==3 and gmBoard[i+a-4][j+a-4] == '.':
                if i+a-4 >=0 and j+a-4 >=0:
                    close+=1
                    break
                
#Check Diagonals
    count = 0;
    for a in range(-4,5):
        if ( i+a >=0 and i+a < N) and (j-a >=0 and j-a < N):
            if a == 0 or (gmBoard[i+a][j-a] == players[ply]):
                if count==3:
                    count = 0
                else:
                    count+=1
            else:
                if count ==3:
                    if gmBoard[i+a][j-a] == players[3-ply]:
                        if i+a-4 >=0 and j-a+4 >=0:
                            if gmBoard[i+a-4][j-a+4] == '.':
                                close= True
                                break
                            else:
                                count = 0
                        else:
                            count = 0
                    elif gmBoard[i+a][j-a] == '.':
                        if i+a-4 >=0 and j-a+4 < N:
                            if gmBoard[i+a-4][j-a+4] == players[3-ply]:
                                close= True
                                break
                            else:
                                count = 0
                        elif i+a-4 == -1 or j-a+4 ==N:
                            close= True
                            break
                        else:
                            count = 0
                        
                else:
                    count =0
        elif i+a == N or j-a ==-1:
            #print(i+a, j+a)
            if count ==3 and gmBoard[i+a-4][j-a+4] == '.':
                if i+a-4 >= 0 and j-a+4 < N:
                    close+=1
                    break
                

#Check for horizontal 
    count = 0;
    for a in range(-4,5):
        if (j+a >=0 and j+a < N):
            if a == 0 or (gmBoard[i][j+a] == players[ply]):
                if count==3:
                    count = 0
                else:
                    count+=1
            else:
                if count ==3:
                    if gmBoard[i][j+a] == players[3-ply]:
                        if j+a-4 >=0:
                            if gmBoard[i][j+a-4] == '.':
                                close+=1
                                break
                            else:
                                count = 0
                        else:
                            count = 0
                    elif gmBoard[i][j+a] == '.':
                        if j+a-4 >=0:
                            if gmBoard[i][j+a-4] == players[3-ply]:
                                close+=1
                                break
                            else:
                                count = 0
                        elif j+a-4 ==-1:
                            close+=1
                            break
                        else:
                            count = 0
                        
                else:
                    count =0
        elif j+a ==N:
            if count ==3 and gmBoard[i][j+a-4] == '.':
                if j+a-4 >=0:
                    close+=1
                    break

#Check for vertical
    count = 0;
    for a in range(-4,5):
        if ( i+a >=0 and i+a < N) :
            if a == 0 or (gmBoard[i+a][j] == players[ply]):
                if count==3:
                    count = 0
                else:
                    count+=1
            else:
                if count ==3:
                    if gmBoard[i+a][j] == players[3-ply]:
                        if i+a-4 >=0 :
                            if gmBoard[i+a-4][j] == '.':
                                close+=1
                                break
                            else:
                                count = 0
                        else:
                            count = 0
                    elif gmBoard[i+a][j] == '.':
                        if i+a-4 >=0:
                            if gmBoard[i+a-4][j] == players[3-ply]:
                                close+=1
                                break
                            else:
                                count = 0
                        elif i+a-4 == -1 :
                            close+=1
                            break
                        else:
                            count = 0
                        
                else:
                    count =0
        elif i+a == N :
            if count ==3 and gmBoard[i+a-4][j] == '.':
                if i+a-4 >=0:
                    close+=1
                    break

    return close
                       
def createClosedTwo(gmBoard, i, j, ply, N):
    
    close = 0
    
    #Check Diagonals
    count = 0;
    for a in range(-3,4):
        if ( i+a >=0 and i+a < N) and (j+a >=0 and j+a < N):
            if a == 0 or (gmBoard[i+a][j+a] == players[ply]):
                if count==2:
                    count = 0
                else:
                    count+=1
            else:
                if count ==2:
                    if gmBoard[i+a][j+a] == players[3-ply]:
                        if i+a-3 >=0 and j+a-3 >=0:
                            if gmBoard[i+a-3][j+a-3] == '.':
                                close+=1
                                break
                            else:
                                count = 0
                        else:
                            count = 0
                    elif gmBoard[i+a][j+a] == '.':
                        if i+a-3 >=0 and j+a-3 >=0:
                            if gmBoard[i+a-3][j+a-3] == players[3-ply]:
                                close+=1
                                break
                            else:
                                count = 0
                        elif i+a-3 == -1 or j+a-3==-1:
                            close+=1
                            break
                        else:
                            count = 0
                        
                else:
                    count =0
        elif i+a == N or j+a ==N:
            if count ==2 and gmBoard[i+a-3][j+a-3] == '.':
                if i+a-3 >=0 and j+a-3 >=0:
                    close+=1
                    break


#Check Diagonals
    count = 0;
    for a in range(-3,4):
        if ( i+a >=0 and i+a < N) and (j-a >=0 and j-a < N):
            if a == 0 or (gmBoard[i+a][j-a] == players[ply]):
                if count==2:
                    count = 0
                else:
                    count+=1
            else:
                if count ==2:
                    if gmBoard[i+a][j-a] == players[3-ply]:
                        if i+a-3 >=0 and j-a+3 >=0:
                            if gmBoard[i+a-3][j-a+3] == '.':
                                close+=1
                                break
                            else:
                                count = 0
                        else:
                            count = 0
                    elif gmBoard[i+a][j-a] == '.':
                        if i+a-3 >=0 and j-a+3 < N:
                            if gmBoard[i+a-3][j-a+3] == players[3-ply]:
                                close+=1
                                break
                            else:
                                count = 0
                        elif i+a-3 == -1 or j-a+3 ==N:
                            close+=1
                            break
                        else:
                            count = 0
                        
                else:
                    count =0
        elif i+a == N or j-a ==-1:
            #print(i+a, j+a)
            if count ==2 and gmBoard[i+a-3][j-a+3] == '.':
                if i+a-3 >= 0 and j-a+3 < N:
                    close+=1
                    break

#Check for horizontal 
    count = 0;
    for a in range(-3,4):
        if (j+a >=0 and j+a < N):
            if a == 0 or (gmBoard[i][j+a] == players[ply]):
                if count==2:
                    count = 0
                else:
                    count+=1
            else:
                if count ==2:
                    if gmBoard[i][j+a] == players[3-ply]:
                        if j+a-3 >=0:
                            if gmBoard[i][j+a-3] == '.':
                                close+=1
                                break
                            else:
                                count = 0
                        else:
                            count = 0
                    elif gmBoard[i][j+a] == '.':
                        if j+a-3 >=0:
                            if gmBoard[i][j+a-3] == players[3-ply]:
                                close+=1
                                break
                            else:
                                count = 0
                        elif j+a-3 ==-1:
                            close+=1
                            break
                        else:
                            count = 0
                        
                else:
                    count =0
        elif j+a ==N:
            if count ==2 and gmBoard[i][j+a-3] == '.':
                if j+a-3 >=0:
                    close+=1
                    break
                
#Check for vertical
    count = 0
    for a in range(-3,4):
        if ( i+a >=0 and i+a < N) :
            if a == 0 or (gmBoard[i+a][j] == players[ply]):
                if count==2:
                    count = 0
                else:
                    count+=1
            else:
                if count ==2:
                    if gmBoard[i+a][j] == players[3-ply]:
                        if i+a-3 >=0 :
                            if gmBoard[i+a-3][j] == '.':
                                close+=1
                                break
                            else:
                                count = 0
                        else:
                            count = 0
                    elif gmBoard[i+a][j] == '.':
                        if i+a-3 >=0:
                            if gmBoard[i+a-3][j] == players[3-ply]:
                                close+=1
                                break
                            else:
                                count = 0
                        elif i+a-3 == -1 :
                            close+=1
                            break
                        else:
                            count = 0
                        
                else:
                    count =0
        elif i+a == N :
            if count ==2 and gmBoard[i+a-3][j] == '.':
                if i+a-3 >=0:
                    close+=1
                    break
    
    return close 



def create_open_four(board, i, j, p, N):
    open = 0
    count = 0
    for m in range(-4,5):
        if outsideBoard(i+m,j,N):                            # for vertical
            if board[i+m][j] == players[p] or m == 0:           # if 'w' increase count by 1
                count += 1
            elif board[i+m][j] == players[3 - p]:               # if 'b' make count 0
                count = 0
            elif board[i+m][j] == '.':
                if count == 5 and outsideBoard(i+m-5,j, N) and board[i+m-5][j] == ".":
                    print("Vertical CreateOpenFour found at " + str(i) + " " + str(j))
                    open+=1
                else:
                    count = 1
            # if count == 6:                                          # confirm it's use
            #     print("Vertical CreateOpenFour found!!")
            #     return True
    count = 0
    for m in range(-4, 5):
        if outsideBoard(i+m, j+m, N):                         # for one diagonal
            if board[i + m][j+m] == players[p] or m == 0:       # if 'w' increase count by 1
                count += 1
            elif board[i + m][j+m] == players[3 - p]:           # if 'b' make count 0
                count = 0
            elif board[i + m][j+m] == '.':
                if count == 5 and outsideBoard(i+m-5,j+m+5,N) and board[i+m-5][j+m-5] == ".":
                    print("One diag CreateOpenFour found at " + str(i) + " " + str(j))
                    open+=1
                else:
                    count = 1
            # if count == 6:
            #     print("One diag CreateOpenFour found!!")
            #     return True
    count = 0
    for m in range(-4, 5):
        if outsideBoard(i + m, j - m ,N):                     # for other diagonal
            if board[i + m][j - m] == players[p] or m == 0:     # if 'w' increase count by 1
                count += 1
            elif board[i + m][j - m] == players[3 - p]:         # if 'b' make count 0
                count = 0
            elif board[i + m][j - m] == '.':
                if count == 5 and outsideBoard(i+m-5,j-m+5, N) and board[i+m-5][j-m+5] == ".":
                    print("Other diagonal CreateOpenFour found at " + str(i) + " " + str(j))
                    open+=1
                else:
                    count = 1
            # if count == 6:
            #     print("Other diag CreateOpenFour found!!")
            #     return True
    count = 0
    for m in range(-4, 5):
        if outsideBoard(i, j + m, N):                         # for horizontal direction
            if board[i][j + m] == players[p] or m == 0:         # if 'w' increase count by 1
                count += 1
            elif board[i][j + m] == players[3 - p]:             # if 'b' make count 0
                count = 0
            elif board[i][j + m] == '.':
                if count == 5 and outsideBoard(i,j+m-5, N) and board[i][j+m-5] == ".":
                    print("Horizontal CreateOpenFour found at " + str(i) + " " + str(j))
                    open+=1
                else:
                    count = 1
            # if count == 6:
            #     print("Horizontal CreateOpenFour found!!")
            #     return True
    return open

def create_open_three(board, i, j, p, N):
    
    open = 0
    count = 0
    for m in range(-3, 4):
        if outsideBoard(i + m, j, N):                     # for vertical
            if board[i + m][j] == players[p] or m == 0:     # if 'w' increase count by 1
                count += 1
            elif board[i + m][j] == players[3 - p]:         # if 'b' make count 0
                count = 0
            elif board[i + m][j] == '.':
                if count == 4 and outsideBoard(i+m-4,j, N) and board[i+m-4][j] == ".":
                    print("Vertical CreateOpenThree found at " + str(i) + " " + str(j))
                    open+=1
                else:
                    count = 1
            # if count == 5:
            #     print("Vertical CreateOpenThree found!!")
            #     return True
    count = 0
    for m in range(-3, 4):
        if outsideBoard(i+m, j+m, N):                         # for one diagonal
            if board[i + m][j+m] == players[p] or m == 0:       # if 'w' increase count by 1
                count += 1
            elif board[i + m][j+m] == players[3 - p]:           # if 'b' make count 0
                count = 0
            elif board[i+m][j+m] == '.':
                if count == 4 and outsideBoard(i+m-4,j+m-4, N) and board[i+m-4][j+m-4] == ".":           # for avoiding 4 'w' in a row
                    print("One diagonal CreateOpenThree found at " + str(i) + " " + str(j))
                    open+=1
                else:
                    count = 1
            # if count == 5:
            #     print("One diagonal CreateOpenThree found!!")
            #     return True
    count = 0
    for m in range(-3, 4):
        if outsideBoard(i+m, j-m, N):                         # for other diagonal
            if board[i+m][j-m] == players[p] or m == 0:         # if 'w' increase count by 1
                count += 1
            elif board[i+m][j-m] == players[3 - p]:             # if 'b' make count 0
                count = 0
            elif board[i+m][j-m] == '.':
                if count == 4 and outsideBoard(i+m-4,j-m+4, N) and board[i+m-4][j-m+4] == ".":
                    print("Other diagonal CreateOpenThree found at " + str(i) + " " + str(j))
                    open+=1
                else:
                    count = 1
            # if count == 5:
            #     print("Other diagonal CreateOpenThree found!!")
            #     return True
    count = 0
    for m in range(-3, 4):
        if outsideBoard(i, j+m, N):                       # for horizontal
            if board[i][j+m] == players[p] or m == 0:       # if 'w' increase count by 1
                count += 1
            elif board[i][j+m] == players[3 - p]:           # if 'b' make count 0
                count = 0
            elif board[i][j+m] == '.':
                if count == 4 and outsideBoard(i,j+m-4, N) and board[i][j+m-4] == ".":
                    print("Horizontal CreateOpenThree found at " + str(i) + " " + str(j))
                    open+=1
                else:
                    count = 1
            # if count == 5:
            #     print("Horizontal CreateOpenThree found at count 5 " + str(i) + " " + str(j))
            #     return True
    return open


def create_open_two(board, i, j, p, N):
    open = 0
    count = 0
    for m in range(-2, 3):
        if outsideBoard(i + m, j, N):                     # for vertical
            if board[i + m][j] == players[p] or m == 0:     # if 'w' increase count by 1
                count += 1
            elif board[i + m][j] == players[3 - p]:         # if 'b' make count 0
                count = 0
            elif board[i + m][j] == '.':
                if count == 3 and outsideBoard(i+m-3,j, N) and board[i+m-3][j] == ".":
                    print("Vertical CreateOpenTwo found at " + str(i) + " " + str(j))
                    open+=1
                else:
                    count = 1
            # if count == 5:
            #     print("Vertical CreateOpenThree found!!")
            #     return True
    count = 0
    for m in range(-2, 3):
        if outsideBoard(i+m, j+m, N):                         # for one diagonal
            if board[i + m][j+m] == players[p] or m == 0:       # if 'w' increase count by 1
                count += 1
            elif board[i + m][j+m] == players[3 - p]:           # if 'b' make count 0
                count = 0
            elif board[i+m][j+m] == '.':
                if count == 3 and outsideBoard(i+m-3,j+m-3, N) and board[i+m-3][j+m-3] == ".":           # for avoiding 3 'w' in a row
                    print("One diagonal CreateOpenTwo found at " + str(i) + " " + str(j))
                    open+=1
                else:
                    count = 1
            # if count == 5:
            #     print("One diagonal CreateOpenThree found!!")
            #     return True
    count = 0
    for m in range(-2, 3):
        if outsideBoard(i+m, j-m, N):                         # for other diagonal
            if board[i+m][j-m] == players[p] or m == 0:         # if 'w' increase count by 1
                count += 1
            elif board[i+m][j-m] == players[3 - p]:             # if 'b' make count 0
                count = 0
            elif board[i+m][j-m] == '.':
                if count == 3 and outsideBoard(i+m-3,j-m+3, N) and board[i+m-3][j-m+3] == ".":               # check once
                    print("Other diagonal CreateOpenTwo found at " + str(i) + " " + str(j))
                    open+=1
                else:
                    count = 1
            # if count == 5:
            #     print("Other diagonal CreateOpenThree found!!")
            #     return True
    count = 0
    for m in range(-2, 3):
        if outsideBoard(i, j+m, N):                       # for horizontal
            if board[i][j+m] == players[p] or m == 0:       # if 'w' increase count by 1
                count += 1
            elif board[i][j+m] == players[3 - p]:           # if 'b' make count 0
                count = 0
            elif board[i][j+m] == '.':
                if count == 3 and outsideBoard(i,j-m+3, N) and board[i][j+m-3] == ".":
                    print("Horizontal CreateOpenTwo found at " + str(i) + " " + str(j))
                    open+=1
                else:
                    count = 1
            # if count == 5:
            #     print("Horizontal CreateOpenThree found at count 5 " + str(i) + " " + str(j))
            #     return True
    return open

def outsideBoard(i , j, N):
    if((i >=0 and i < N) and ( j >=0 and j < N)):
        return True
    else:
        return False
    
    
    
    
    
    
        