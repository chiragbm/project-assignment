'''
Created on May 29, 2016

@author: DELL
'''
import sys
import re
from Moves import checkWin
from _overlapped import NULL
from Game.Moves import blockClosedFour, createClosedFour

# Always use index by reducing it's value by 1
# This are Columns - indices.

'''
check for multiple states for single value
'''
alpha = { 1:"A", 2:"B",3:"C",4:"D",5:"E",6:"F",7:"G",8:"H",9:"I",10:"J",
          11:"K",12:"L",13:"M",14:"N",15:"O",16:"P",17:"Q",18:"R",
          19:"S",20:"T",21:"U",22:"V",23:"W",24:"X",25:"Y"}

players = { 1:"b",
           2:"w"
           }

def createBoard(gmBoard, f, N):
    
    for i in range(0,int(N)):
        gmBoard.append(f.readline())
    gmBoard.reverse() 
    return gmBoard


def getLegalMoves(gmBoard, legalMoves, N):
    
    for i in range(0, int(N)):
        for j in range(0, int(N)):
            if gmBoard[i][j] != '.' :
                if i-1 >= 0 and j-1 >=0 :
                    legalMoves = appendLegalMoveList(gmBoard, legalMoves, i-1, j-1)
#                     
                if j-1>=0:
                    legalMoves = appendLegalMoveList(gmBoard, legalMoves, i, j-1)
#                     
                if i+1 < N and j-1>=0:
                    legalMoves = appendLegalMoveList(gmBoard, legalMoves, i+1, j-1)
#                     
                if i-1>=0:
                    legalMoves = appendLegalMoveList(gmBoard, legalMoves, i-1, j)
#                     
                if i+1 <N :
                    legalMoves = appendLegalMoveList(gmBoard, legalMoves, i+1, j)
#                     
                if i-1>=0 and j+1<N:
                    legalMoves = appendLegalMoveList(gmBoard, legalMoves, i-1, j+1)
#                     
                if j+1<N:
                    legalMoves = appendLegalMoveList(gmBoard, legalMoves, i, j+1)
#                     
                if i+1<N and j+1<N:
                    legalMoves = appendLegalMoveList(gmBoard, legalMoves, i+1, j+1)
#                     
    return legalMoves

def appendLegalMoveList(gmBoard, legalMoves ,indI, indJ):
    
    if gmBoard[indI][indJ] == ".":
        move  = alpha[indJ+1]+str(indI+1)
        if move not in legalMoves:
            legalMoves.append(move)
    return legalMoves


def sortAlphaNumerically( l ):
    
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key = alphanum_key)


def evaluateMoves(legalMoves, gmBoard, ply):
    gmax = -1
    #min = 0
    finalMove = NULL
    for move in legalMoves:
        lmax = 0
        print("Evaluate: %s"%move)
        indices = getIndices(move)
        i = indices[1]
        j = indices[0]
        print("Indices: %s, %s"%(i,j))
#         win = checkWin(gmBoard, i, j, ply, N)
#         if win :
#             lmax = lmax + 50000
#         print ("win = ", win)
#         block = blockClosedFour(gmBoard, i, j, ply, N)
#         if block:
#             lmax = lmax + 10000
#         print ("block = ", block)
        
        close = createClosedFour(gmBoard, i, j, ply, N)
        if close:
            lmax = lmax + 1000
        print ("close = ", close)
            
        if lmax > gmax:
            gmax = lmax
            finalMove = move
        
    return finalMove



def getIndices(move):
    indices = list()
    key = [key for key, value in alpha.items() if value == move[0:1]][0]
    indices.append(int(key) - 1)
    indices.append(int(move[1:])-1)
    return indices


gmBoard = []
legalMoves = list()

f = open(sys.argv[1], "r")
algo = f.readline()
player = int(f.readline())
depth = f.readline()
N = int(f.readline())

gmBoard = createBoard(gmBoard, f, N)
legalMoves = getLegalMoves(gmBoard, legalMoves, N)
legalMoves = sortAlphaNumerically(legalMoves)

print(legalMoves)
print(len(legalMoves))

move = evaluateMoves(legalMoves, gmBoard, player)
print(move)
print(players[player])



'''
Need to add two functions
1) Find Legal Moves
2) Add values for evaluation function.
2) Evaluate function based on greedy approach for a mentioned player.
'''
