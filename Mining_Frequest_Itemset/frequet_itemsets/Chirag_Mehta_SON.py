'''
Created on Feb 24, 2017

@author: chira
'''
from pyspark import SparkConf, SparkContext
import time
import itertools
import sys
from _collections import defaultdict

"""
user, movie
case 1: user = {unique movies}
case 2: movies = {unique users}

"""

import operator as op
def ncr(n, r):
    r = min(r, n-r)
    if r == 0: return 1
    numer = reduce(op.mul, xrange(n, n-r, -1))
    denom = reduce(op.mul, xrange(1, r+1))
    return numer//denom


def toivonen_2(iterator):
    
    list_of_baskets = []
    sup = partition_support
    sample_freq_itemset = list()
    unique_singleton_items = list()
    
    item_count = {}
    for i in iterator:
        for element in i[1]:
            if item_count.get(element):
                item_count[element] += 1
            else:
                item_count[element] = 1
        
        list_of_baskets.append(i[1])
    
    for item in item_count:
        if item_count[item] >= sup:
            unique_singleton_items.append(item)
            sample_freq_itemset.append((item,1))
    
    candidate_set = set()
    for subset in itertools.combinations(unique_singleton_items, 2):
        
        subset = frozenset(subset)
        count = 0
        for basket in list_of_baskets:
            if subset.issubset(basket):
                count+=1
        
        if count >=sup:
            candidate_set.add(subset)
            sample_freq_itemset.append((subset,1))
        
    unique_singleton_items = list(candidate_set)
    
    '''
    
    k = 3
    while len(unique_singleton_items)!=0:

        C = ncr(k,2)
        
        
        temp_sets = set()
        candidate_set = set()
        candidate_dict = defaultdict(lambda : 0)
        for subset in itertools.combinations(unique_singleton_items, 2):
            
            subset = set(subset)
            
            subset = set().union(*subset)
                
            if len(subset) == k:
                temp_str = ''
                for s in sorted(subset):
                    temp_str+=str(s)
                  
#                 print temp_str  
                candidate_dict[temp_str]+=1
                    
                if candidate_dict[temp_str] == C:
#                     print candidate_dict[temp_str]
                    temp_sets.add(frozenset(subset))
        
        for item_set in temp_sets:
            count = 0
            for basket in list_of_baskets:
                if item_set.issubset(basket):
                    count+=1
            
            if count >=sup:
                candidate_set.add(item_set)
                sample_freq_itemset.append((item_set,1))
        
        unique_singleton_items = list(candidate_set)
        k+=1
    '''
    return iter(sample_freq_itemset)


def count_candidates_values(iterator):
    
#     print 'Count Candidates'
    candidate_list = []
    partition_list = []
    
    for i in iterator:
        partition_list.append(i[1])
    
#     print partition_list
    
    for value in map_data_1:
        count = 0
        if type(value[0]) == type(1):
            for i in partition_list:
                if value[0] in i:
                    count+=1
        else:
            for i in partition_list:
                if value[0].issubset(i):
                    count+=1
        
        candidate_list.append((value[0], count))
    
    return iter(candidate_list)


################################## ----------------------- Start ---------------------------------###############################################
## Create Data
start = time.time()
conf = SparkConf().setAppName("Frequent_Itemsets_2")
sc = SparkContext(conf = conf)

case = sys.argv[1]
file_name = sys.argv[2]
support = int(sys.argv[3])
# output_file = 'output.txt'
output_file = 'chirag_mehta_SON_MovieLens.Small.case2-200.txt'

data = sc.textFile(file_name)
partition_support =  support / data.getNumPartitions()
header = data.first()
data = data.filter(lambda x : x!=header)

if case == '1':
    data = data.map(lambda x : (int(x.split(',')[0]), int(x.split(',')[1])) ).groupByKey().mapValues(set)
elif case == '2':
    data = data.map(lambda x : (int(x.split(',')[1]), int(x.split(',')[0])) ).groupByKey().mapValues(set)
                        

map_data_1 = data.mapPartitions(toivonen_2).reduceByKey(lambda a, b : a+b ).collect()

map_data_2 = data.mapPartitions(count_candidates_values).reduceByKey(lambda a, b : a+b ).collect()

final_frequent_items_dict = defaultdict(lambda : [])

for d in map_data_2:
    if d[1] >= support:
        if type(d[0]) == type(1):
            final_frequent_items_dict[1].append(d[0])
        else:
            final_frequent_items_dict[len(d[0])].append(sorted(list(d[0])))

out = open(output_file, 'w')

c = 0

for key in sorted(final_frequent_items_dict.keys()):
    
    comma_boolean = True
    main_str = ''
    c+=len(final_frequent_items_dict[key])
    for value in sorted(final_frequent_items_dict[key]):
        
        s=''
        if type(value) == type(1):
            if not comma_boolean:
                s+=',('+str(value)+')'
            else:
                s+='('+str(value)+')'
                comma_boolean= False
            
            main_str+=s
        else:
            
            internal_comma = True
            internal_list_str = ''
            for v in value:
                if not internal_comma:
                    internal_list_str+=','+str(v)
                else:
                    internal_list_str+=str(v)
                    internal_comma = False
            
            if not comma_boolean:
                s+=',('+internal_list_str+')'
            else:
                s+='('+internal_list_str+')'
                comma_boolean= False
            main_str+=s  
    
    print main_str
    print "hello"  
            
#     out.write("%s\n"%main_str)


print 'No of frequent itemset generated = ', c
out.close()
print 'Time Taken  :', time.time() - start







