'''
Created on Sep 11, 2016

@author: chira
'''
'''
0 - ham
1 - spam

spam file
ham file
spam word
ham word

'''
import sys
import os
from math import log
import re

def filter_words(word):
    
    filter_chars = ['?','!', '.', "/" , '@', '_' , '|', '\\', '_',  '$', '*', ')', '(', '{', '}', '[', ']', '~']
         
    for char in filter_chars:
         
        rg = r"(\%s+|\w+\%s{2,})"%(char,char)
        m = re.fullmatch(rg, word)
         
        if(m != None):
            return True
     
    return False
        

def get_word_prob_ham(word):
    
    if vocabulary.get(word) == None:
        return 'ignore'

    word_count_ham = vocabulary[word][0]
    word_prob = (word_count_ham + 1) / (ham_word_count + vocab_count)
    return log(word_prob)

def get_word_prob_spam(word):
    
    if vocabulary.get(word) == None:
        return 'ignore'

    word_count_spam = vocabulary[word][1]
    word_prob = (word_count_spam + 1) / (spam_word_count + vocab_count)
    return log(word_prob)

def process_file(filepath):
    f = open(filepath, 'r',encoding = 'latin1')
    
    ham_msge_prob = 0
    spam_msge_prob = 0
        
    while(True):
        line = f.readline()
        
        if(line == ""):
            break
        
        words = line.split()
        
        for word in words:
            
            if(not filter_words(word)):

                ham_word_prob = get_word_prob_ham(word)
                spam_word_prob = get_word_prob_spam(word)
                
                if(ham_word_prob != 'ignore' and spam_word_prob !='ignore'):
                    
                    ham_msge_prob+=ham_word_prob
                    spam_msge_prob+=spam_word_prob
                    
    f.close()        
    if ham_msge_prob != 0 and spam_msge_prob != 0:
        
        spam_document_prob = spam_msge_prob + log(spam_prob)
        ham_document_prob = ham_msge_prob + log(ham_prob)
        
        return spam_document_prob, ham_document_prob
    
    return 'ignore', 'ignore'
    
def calculate_precision(correct_classified, total_classified):
    return correct_classified / total_classified

def calculate_recall(correct_classified, totat_belongs_class):
    return correct_classified / totat_belongs_class

def calculate_f1(correct_classified, total_classified, totat_belongs_class):
    
    prec = calculate_precision(correct_classified, total_classified)
    recall = calculate_recall(correct_classified, totat_belongs_class)
    
    f1 = (2 * prec * recall) / (prec + recall)
    
    return f1

# ------------------------------- create model

vocabulary = {}
spam_file_count = 0
ham_file_count = 0
nbmodel = open("nbmodel.txt","r", encoding = 'latin1')

spam_file_count = int((nbmodel.readline()).strip())
ham_file_count = int((nbmodel.readline()).strip())
spam_word_count = int((nbmodel.readline()).strip())
ham_word_count = int((nbmodel.readline()).strip())

spam_prob = spam_file_count / (spam_file_count + ham_file_count)
ham_prob = ham_file_count / (spam_file_count + ham_file_count)

vocab_count = int((nbmodel.readline()).strip())

while(True):
    line = nbmodel.readline()
      
    if(line==""):
        break
     
    arr = line.split()
    word = arr[0].strip()
    ham = int(arr[1].strip())
    spam = int(arr[2].strip())
    
    spam_and_ham = []
    spam_and_ham.append(ham)
    spam_and_ham.append(spam)
    
    vocabulary[word] = spam_and_ham

nbmodel.close()

#------------------------------------- Perform Naive Bayes
'''

- get directory path, walk through it.
- open writer file
- Read all files : process each one and apply naive bayes: Spam and Ham
- Compare each probs
- add file name and label it.
- close it.

Accuracy:
    count(no_of_doc_crct_class) / count(no of docs)
Precision:
    count(no_of_doc_crct_class) / count(docs_classified_ck
recall ck:
    

'''

real_spam_file_count = 0
real_ham_file_count = 0
correct_classified_spam = 0
correct_classified_ham = 0
file_classified_ham = 0
file_classified_spam = 0

out = open('nboutput.txt', 'w+')
 
d_path = sys.argv[1]
 
 
dirs = os.listdir(d_path)
is_spam = False
 
for directory, subdirectories, files in os.walk(d_path):
 
    for file in files:
        if file.endswith('.txt'):
            filename = os.path.join(directory, file)
            spam, ham = process_file(filename)
            
            if spam != 'ignore' and ham != 'ignore':
                if(spam > ham):
                    is_spam = True
                    file_classified_spam +=1
                    out.write("spam %s\n"%(filename))
                else:
                    is_spam = False
                    file_classified_ham +=1
                    out.write("ham %s\n"%(filename))
            
            if directory.endswith('ham'):
                
                if not is_spam:
                    correct_classified_ham+=1
                real_ham_file_count +=1
                
                
            elif directory.endswith('spam'):
                
                if is_spam:
                    correct_classified_spam+=1
                real_spam_file_count+=1
            
            
 
out.close()

print('real_spam_file_count', real_spam_file_count)
print('real_ham_file_count', real_ham_file_count)
print('file_classified_spam', file_classified_spam)
print('file_classified_ham', file_classified_ham)
print('correct_classified_spam', correct_classified_spam)
print('correct_classified_ham', correct_classified_ham)
print()
print('class\tprecision recall f1')
print('spam\t%.2f\t  %.2f\t %.2f'%(calculate_precision(correct_classified_spam, file_classified_spam),calculate_recall(correct_classified_spam, real_spam_file_count), calculate_f1(correct_classified_spam, file_classified_spam, real_spam_file_count)))
print('ham\t%.2f\t  %.2f\t %.2f'%(calculate_precision(correct_classified_ham, file_classified_ham),calculate_recall(correct_classified_ham, real_ham_file_count), calculate_f1(correct_classified_ham, file_classified_ham, real_ham_file_count)))




    
    
