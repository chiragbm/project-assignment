'''
Created on Sep 11, 2016

@author: chira
'''
'''
0 - ham
1 - spam

spam file
ham file
spam word
ham word

'''
import sys
import os
from math import log


def get_word_prob_ham(word):
    
    if vocabulary.get(word) == None:
        return 'ignore'

    word_count_ham = vocabulary[word][0]
    word_prob = (word_count_ham + 1) / (ham_word_count + vocab_count)
    return log(word_prob)

def get_word_prob_spam(word):
    
    if vocabulary.get(word) == None:
        return 'ignore'

    word_count_spam = vocabulary[word][1]
    word_prob = (word_count_spam + 1) / (spam_word_count + vocab_count)
    return log(word_prob)

def process_file(filepath):
    f = open(filepath, 'r',encoding = 'latin1')
    
    ham_msge_prob = 0
    spam_msge_prob = 0
        
    while(True):
        line = f.readline()
        
        if(line == ""):
            break
        
        words = line.split()
        
        for word in words:
            
            ham_word_prob = get_word_prob_ham(word)
            spam_word_prob = get_word_prob_spam(word)
            
            if(ham_word_prob != 'ignore' and spam_word_prob !='ignore'):
                
                #if(ham_msge_prob != -1):
                ham_msge_prob+=ham_word_prob
                #else:
                #    ham_msge_prob = ham_word_prob
                    
                #if(spam_msge_prob != -1):
                spam_msge_prob+=spam_word_prob
                #else:
                #    spam_msge_prob = spam_word_prob
                    
    f.close()        
    if ham_msge_prob != 0 and spam_msge_prob != 0:
        
        spam_document_prob = spam_msge_prob + log(spam_prob)
        ham_document_prob = ham_msge_prob + log(ham_prob)
        
        return spam_document_prob, ham_document_prob
    
    return 'ignore', 'ignore'
    

# ------------------------------- create model

vocabulary = {}
spam_file_count = 0
ham_file_count = 0
nbmodel = open("nbmodel.txt","r", encoding = 'latin1')

spam_file_count = int((nbmodel.readline()).strip())
ham_file_count = int((nbmodel.readline()).strip())
spam_word_count = int((nbmodel.readline()).strip())
ham_word_count = int((nbmodel.readline()).strip())

spam_prob = spam_file_count / (spam_file_count + ham_file_count)
ham_prob = ham_file_count / (spam_file_count + ham_file_count)

vocab_count = int((nbmodel.readline()).strip())

while(True):
    line = nbmodel.readline()
      
    if(line==""):
        break
     
    arr = line.split()
    word = arr[0].strip()
    ham = int(arr[1].strip())
    spam = int(arr[2].strip())
    
    spam_and_ham = []
    spam_and_ham.append(ham)
    spam_and_ham.append(spam)
    
    vocabulary[word] = spam_and_ham

nbmodel.close()

#------------------------------------- Perform Naive Bayes
'''

- get directory path, walk through it.
- open writer file
- Read all files : process each one and apply naive bayes: Spam and Ham
- Compare each probs
- add file name and label it.
- close it.

Accuracy:
    count(no_of_doc_crct_class) / count(no of docs)
Precision:
    count(no_of_doc_crct_class) / count(docs_classified_ck
recall ck:
    

'''

out = open('nboutput.txt', 'w+')
 
d_path = sys.argv[1]
 
 
dirs = os.listdir(d_path)
 
for directory, subdirectories, files in os.walk(d_path):
 
    for file in files:
        if file.endswith('.txt'):
            filename = os.path.join(directory, file)
            spam, ham = process_file(filename)
             
            if spam != 'ignore' and ham != 'ignore':
                if(spam > ham):
                    out.write("spam %s\n"%(filename))
                else:
                    out.write("ham %s\n"%(filename))
 
out.close()


    
    
