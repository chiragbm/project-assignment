'''
Created on Sep 10, 2016

@author: chira
'''

import sys
import os
import re

'''
0 - ham
1 - spam

'''

def filter_words(word):
    
    filter_chars = ['?','!', '.', "/" , '@', '_' , '|', '\\', '_',  '$', '*', ')', '(', '{', '}', '[', ']', '~']
         
    for char in filter_chars:
         
        rg = r"(\%s+|\w+\%s{2,})"%(char,char)
        m = re.fullmatch(rg, word)
         
        if(m != None):
            return False
     
    return True

def process_ham_file(filename):
    global ham_word_count
    f = open(filename, 'r', encoding = 'latin1')
    while(True):
        line = f.readline()
        if(line==""):
            break
        words = line.split()
        for word in words:
            
            #if(filter_words(word)):
            
            ham_word_count+=1
            
            if vocabulary.get(word)!= None:
                vocabulary[word][0]+=1
            else:
                vocab = []
                vocab.append(1)
                vocab.append(0)
                vocabulary[word] = vocab
    f.close()


def process_spam_file(filename):
    global spam_word_count
    f = open(filename, 'r', encoding='latin1')
    while(True):
        line = f.readline()
        if(line==""):
            break
        words = line.split()
        for word in words:
            
#             if filter_words(word):
            
            spam_word_count+=1
            if vocabulary.get(word)!= None:
                vocabulary[word][1]+=1
            else:
                vocab = []
                vocab.append(0)
                vocab.append(1)
                vocabulary[word] = vocab
    f.close()

# ------------------------------------- Starts Here --------------------------------

spam_file_count = 0
ham_file_count = 0
spam_word_count = 0
ham_word_count = 0
vocabulary = {}

path = sys.argv[1]

for dirNames, subDirList, files in os.walk(path):
    
    # process ham files
    if dirNames.endswith("ham"):
        for file in os.listdir(dirNames):
            if file.endswith(".txt"):
                ham_file_count +=1
                process_ham_file(dirNames+'//'+file)    
    
    # process spam files
    elif dirNames.endswith("spam"):
        for file in os.listdir(dirNames):
            if file.endswith('.txt'):
                spam_file_count+=1
                process_spam_file(dirNames+'//'+file)

#-----------------------------------Print model

out = open('nbmodel.txt', 'w+', encoding = "latin1")

out.write("%s\n"%spam_file_count)
out.write("%s\n"%ham_file_count)
out.write("%s\n"%spam_word_count)
out.write("%s\n"%ham_word_count)

out.write("%s\n"%len(vocabulary.keys()))
for key in vocabulary.keys():
    out.write("%s %s %s\n"%(key,vocabulary[key][0],vocabulary[key][1]))
    
out.close()