'''
Created on Apr 13, 2017
 
@author: chira
'''
 
from _collections import defaultdict
from operator import itemgetter
from pyspark import SparkConf, SparkContext
import sys
import time


def get_connected_components(root, gp):
    
    main_list = list()
    nodes_list = [root]
    nodes_covered = set()
    nodes_covered.add(root)
    
    while(len(nodes_list) != 0 ):
        next_level_list = list()
        for node in nodes_list:
            for child in gp[node]:
                if not nodes_covered.__contains__(child):
                    next_level_list.append(child)
                    nodes_covered.add(child)
        
        main_list.append(nodes_list)
        nodes_list = next_level_list
    
    all_nodes_list = [item for sublist in main_list for item in sublist]
    
    return all_nodes_list

def draw_bfs_tree(gp, root):
    
    main_list = list()
    nodes_list = [root]
    child_parent_dict = {}
    child_parent_dict[root] = [[],0,1,'r',0]
    
    while(len(nodes_list) != 0 ):
        next_level_list = list()
        for node in nodes_list:
            for child in gp[node]:
                if child_parent_dict.__contains__(child):
                    if child_parent_dict[child][1] == child_parent_dict[node][1] + 1:
                        child_parent_dict[child][0].append(node)
                        child_parent_dict[child][2] += child_parent_dict[node][2]
                        child_parent_dict[node][3] = 'm'
                else:
                    child_parent_dict[child] = [[node],child_parent_dict[node][1]+1,child_parent_dict[node][2],'l',0]
                    next_level_list.append(child)
                    child_parent_dict[node][3] = 'm'
        
        main_list.append(nodes_list)
        nodes_list = next_level_list
        
    for i in range(len(main_list)-1, -1, -1):
        nodes_list = main_list[i]
         
         
        for node in nodes_list:
            if child_parent_dict[node][3] == 'l':
                child_parent_dict[node][4] = 1.0
                for parent in child_parent_dict[node][0]:
                    l = sorted([parent, node])
                    ed_str = str(l[0]) +'-'+ str(l[1])
                    val = child_parent_dict[parent][2] / float(child_parent_dict[node][2])
                    edges[ed_str] += val
                    child_parent_dict[parent][4] += val
                      
              
            if child_parent_dict[node][3] == 'm':
                child_parent_dict[node][4] += 1.0
                for parent in child_parent_dict[node][0]:
                    l = sorted([parent, node])
                    ed_str = str(l[0]) + '-' +str(l[1])
                    val = child_parent_dict[parent][2] / float(child_parent_dict[node][2])
                    edges[ed_str] += val * child_parent_dict[node][4]
                    child_parent_dict[parent][4] +=  val * child_parent_dict[node][4]
     
#     for key in child_parent_dict:
#         print key,child_parent_dict[key]
     
#     for key in edges:
#         print key, edges[key]


def update_components(add_comp, remove_comp):
    
    components[add_comp][0] = components[add_comp][0].union(components[remove_comp][0])
    for val in list(components[remove_comp][0]):
        components[val][1] = add_comp
    
    component_labels.remove(remove_comp)

def calc_modularity(labels, compo_set):
    global global_max
    global global_components_list
    
    comps = []
    sum = 0.0
    for s in list(labels):
        comps.append(compo_set[s][0])
        for node_i in list(compo_set[s][0]):
            for node_j in list(compo_set[s][0]):
                if node_i != node_j:
                    a_i_j = 0
                    if node_i in new_graph[node_j]:
                        a_i_j  = 1
                        
                    k_i = node_degrees[node_i]
                    k_j = node_degrees[node_j]
                    expected_edges = (k_i * k_j) / m_2
                    sum += a_i_j - expected_edges
    
    sum = sum / m_2
    
    if sum > global_max:
        global_max = sum
        global_components_list = comps
        
    
    print "No of components: ",len(labels)
    print 'Modularity:', sum
    

####################################### ----- Starts Here ----- ##############################################
start = time.time()
conf = SparkConf().setAppName("Community_Detection")
sc = SparkContext(conf = conf)

file_name = sys.argv[1]
output_file = sys.argv[2]

data = sc.textFile(file_name)
header = data.first()
data = data.filter(lambda x : x!=header)

data = data.map(lambda x : (int(x.split(',')[0]), int(x.split(',')[1])) ).groupByKey().mapValues(set).collectAsMap()
  
keys = sorted(data.keys())

edges = {}
graph = defaultdict(lambda : [])
# 
for i in range(0, len(keys)):
    for j in range(i+1 , len(keys)):
        if len(data[keys[i]].intersection(data[keys[j]])) >= 3:
              
            edges[str(keys[i]) + '-' + str(keys[j])] = 0
              
            graph[keys[i]].append(keys[j])
            graph[keys[j]].append(keys[i])


node_degrees = {}
component_labels = set()
components = {}

for key in graph:
    component_labels.add(key)
    components[key] = [set([key]),key]
    node_degrees[key] = len(graph[key])
    draw_bfs_tree(graph, key)

edges_triples = list()

for key in edges:
    l = key.split("-")
    edges_triples.append((int(l[0]), int(l[1]), edges[key]/2))

edges_triples = sorted(edges_triples, key = lambda t : (t[2]))

# for values in edges_triples:
#     print values

# for node in node_degrees:
#     print node, node_degrees[node]




'''
#### Now modularity comes 
'''


global_components_list = []
global_max = -1000

m_2 = 2*float(len(edges))
prev_connected_components_set = component_labels
prev = None
i = 0

new_graph = defaultdict(lambda : set())

while i < len(edges_triples):
    label_1 = components[edges_triples[i][0]][1]
    label_2 = components[edges_triples[i][1]][1]
    
    new_graph[edges_triples[i][0]].add(edges_triples[i][1])
    new_graph[edges_triples[i][1]].add(edges_triples[i][0])
    
    if label_1 < label_2:
        update_components(label_1, label_2)
        calc_modularity(component_labels, components)
    elif label_2 < label_1:
        update_components(label_2, label_1)
        calc_modularity(component_labels, components)
    i+=1

# print len(global_components_list)
# print global_max
# print global_components_list

global_components_list.sort()

output = open(output_file, 'w')
for l in global_components_list:
    output.write('[')
    first = True
    for val in sorted(l):
        if not first:
            output.write(',%s'%val)
        else:
            first = False
            output.write('%s'%val)
    output.write(']\n')

output.close()
    

# print time.time() - start
