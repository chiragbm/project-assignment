import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs._
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.mllib.recommendation.ALS
import org.apache.spark.mllib.recommendation.Rating
import org.apache.spark.rdd.RDD.doubleRDDToDoubleRDDFunctions
import org.apache.spark.rdd.RDD.rddToPairRDDFunctions


object Chirag_Mehta_task1 {
  def main(args : Array[String]){
    
    val conf = new SparkConf().setAppName("RecommendationSystemTask1").setMaster("local");
    val sc = new SparkContext(conf);
    
    // get data
    var traindata = sc.textFile(args(0))
    var testdata = sc.textFile(args(1))
    
    // get header
    val header = traindata.first();
    
    //filter header from test and train data
    traindata = traindata.filter(x => x != header)
    testdata = testdata.filter(x => x!= header)
    
    // map train data to user, movie, ratings :: Training data 
    val train_ratings = traindata.map(x => (x.split(",")(0),x.split(",")(1),x.split(",")(2)) match {case (user, movie, rate ) => Rating(user.toInt, movie.toInt, rate.toDouble)})
    
    // map test data to user, movie, ratings
    val test_ratings = testdata.map(_.split(",") match {case Array(user, movie, rate) => Rating(user.toInt, movie.toInt, rate.toDouble) })//.sortBy(row => (row.user, row.product))
    
    val test_data = test_ratings.map({case Rating(user, movie, rate) => (user, movie) })
    

    //training parameters
    val rank = 6
    val iterations = 15
    val model = ALS.train(train_ratings, rank, iterations, 0.01)
    
    
    var predictions = model.predict(test_data).map({ case Rating(user, movie, rating) => ((user, movie), rating)})//.sortBy(f => (f._1, f._2)) 
    
    var a =0
    var b =0
    var c =0
    var d =0
    var e =0
    var count = 0
   
    
    var rates_preds = test_ratings.map{case Rating(user, movie, rating) => ((user, movie), rating)}.leftOuterJoin(predictions).map({case((user, movie), (r1, r2))=> ((user, movie),(r1.toDouble, r2.getOrElse(3.0).toDouble))}).sortBy(pair => (pair._1._1, pair._1._2))
    
    rates_preds.map(f => f._1._1+","+f._1._2+","+f._2._2).saveAsTextFile("CM_output.txt")
    merge("CM_output.txt", "Chirag_Mehta_task1.txt")
    
    var l = rates_preds.count()
    
    for( k <- rates_preds.collect()){
      var diff = Math.abs(k._2._1 - k._2._2)
      
      if(diff >=0 && diff <1){
        a+=1
      }else if(diff <2){
        b+=1
      }else if(diff < 3){
        c+=1
      }else if(diff < 4){
        d+=1
      }else{
        e+=1
      }
      count+=1
      if(count == l){
        println(">=0 and <1:"+a)
        println(">=1 and <2:"+b)
        println(">=2 and <3:"+c)
        println(">=3 and <4:"+d)
        println(">=4:"+e)
      }
      
    }
    
    val RMSE = math.sqrt(rates_preds.map{case((user, movie), (r1, r2)) => 
    val err = r1- r2
          err*err
          
    
    }.mean())
    
    println("RMSE ="+RMSE)
  }
  
  
  def merge(srcPath: String, dstPath: String): Unit =  {

    val hadoopConfig = new Configuration()

    val hdfs = FileSystem.get(hadoopConfig)

    FileUtil.copyMerge(hdfs, new Path(srcPath), hdfs, new Path(dstPath), false, hadoopConfig, null)
    hdfs.delete(new Path(srcPath), true)
  }
  
  
}