'''
Created on Mar 16, 2017

@author: chira
'''

from pyspark.conf import SparkConf
from pyspark.context import SparkContext
import sys
import time

def calc_avg(movies_data):
    
    avg_val_dict = {}
    
    for movie in movies_data:
        sum = 0
        for user in movies_data[movie]:
            sum += movies_data[movie][user]
        avg_val_dict[movie] = sum / len(movies_data[movie]) 
    
    return avg_val_dict

def calc_pearson_correlation(movies_data, avg_data):
    
    movie_similarity = {}
    
    for movie in movies_data:
        movie_user_set = set(movies_data[movie].keys())
        movie_similarity[movie] = {}
        for rest_movie in movies_data:
            
            if rest_movie == movie:
                continue
            
            elif rest_movie not in movie_similarity:
                rest_movie_user_set = set(movies_data[rest_movie].keys())
                common_users = rest_movie_user_set.intersection(movie_user_set)
                num = 0
                denom_movie = 0
                denom_rest_movie = 0
                for user in common_users:
                    val1 = movies_data[movie][user] - avg_data[movie]
                    val2 = movies_data[rest_movie][user] - avg_data[rest_movie]
                    num+= val1 * val2
                    denom_movie += val1 ** 2
                    denom_rest_movie += val2 ** 2
                
                
                denom = (denom_movie ** 0.5) * (denom_rest_movie ** 0.5)
                if denom != 0:
                    movie_similarity[movie][rest_movie] = num / denom
                else:
                    movie_similarity[movie][rest_movie] = 0
    
    return movie_similarity


def getPredictions_geners(u, m):
    
    similar_movies = list()
    
    for movie in movie_generes_data:
        
        if movie == m or movie not in movie_user_data:
            continue
        
        if len(set(movie_generes_data[movie]).intersection(set(movie_generes_data[m]))) > 0 :
            similar_movies.append(movie)
    
    num = 0
    denom = 0
    for movie in similar_movies:
        
        user_movie_rating = 0
        movie_m_similarity = 1
#         if(m in movies_similarity_weight[movie]):
#             movie_m_similarity = movies_similarity_weight[movie][m]
#         else :
#             movie_m_similarity = movies_similarity_weight[m][movie]
            
        if u in movie_user_data[movie]:
            user_movie_rating = movie_user_data[movie][u]
            denom+= abs(movie_m_similarity)

        num += movie_m_similarity * user_movie_rating
    
    if denom != 0:
        return num / denom
    else:
        return getPredictions(u, m)
#         num = 0
#         denom = 0
#         for movie in movie_user_data:
#             if u in movie_user_data[movie]:
#                 num += movie_user_data[movie][u]
#                 denom += 1
#           
#         if denom != 0:
#             return num / denom
#         else:
#             return 0
#         return 0
        


def getPredictions(u, m):
    
    movie_weight_tupes = list()
    for movie in movie_user_data:
        
        if movie == m:
            continue
        
        if movie in movies_similarity_weight[m]:
            movie_weight_tupes.append((movie, movies_similarity_weight[m][movie])) 
        else:
            movie_weight_tupes.append((movie, movies_similarity_weight[movie][m]))
     
    movie_weight_tupes.sort(key = lambda x : x[1], reverse = True)
    
    num = 0
    denom = 0
    for i in range(4000):
        
        user_movie_rating = 0
        
        if u in movie_user_data[movie_weight_tupes[i][0]]:
            user_movie_rating = movie_user_data[movie_weight_tupes[i][0]][u]
            denom+= abs(movie_weight_tupes[i][1])

        num += movie_weight_tupes[i][1] * user_movie_rating
    
    if denom != 0:
        return num / denom
    else:
        num = 0
        denom = 0
        for movie in movie_user_data:
            #if len(set(movie_generes_data[movie]).intersection(set(movie_generes_data[m]))) > 0 :
            if u in movie_user_data[movie]:
                num += movie_user_data[movie][u]
                denom += 1
         
        if denom != 0:
            return num / denom
        else:
            return 0
    
####################### ----------------------Start Here --------------------------###############################
start = time.time()
conf = SparkConf().setAppName("Recommendatian_Systems")
sc = SparkContext(conf = conf)

training_data = sys.argv[1]
testing_data = sys.argv[2]
movie_data = sys.argv[3]


training_data = sc.textFile(training_data)
header = training_data.first()
training_data = training_data.filter(lambda x : x!=header)

movie_user_data = training_data.map(lambda x: (int(x.split(',')[1]),(int(x.split(',')[0]),float(x.split(',')[2])))).groupByKey().mapValues(dict).collectAsMap()

movie_generes_data = sc.textFile(movie_data)
header = movie_generes_data.first()
movie_generes_data = movie_generes_data.filter(lambda x : x!=header)

movie_generes_data = movie_generes_data.map(lambda x: (int(x.split(',')[0]), x.split(',')[2].split("|"))).collectAsMap()

#### Calc avg
movies_avg_ratings = calc_avg(movie_user_data)

#### Calc similarity
movies_similarity_weight = calc_pearson_correlation(movie_user_data, movies_avg_ratings)

testing_data = sc.textFile(testing_data)
header = testing_data.first()
testing_data = testing_data.filter(lambda x : x!=header)


user_movies_test = testing_data.map(lambda x: (int(x.split(',')[0]),(int(x.split(',')[1]),float(x.split(',')[2])))).groupByKey().sortByKey().mapValues(dict).collectAsMap()

output_file = open('Chirag_Mehta_result.txt', 'w')
output_file.write("UserId,MovieId,Pred_rating\n")

acc = 0
n = 0

l = [0,0,0,0,0]
for user in user_movies_test:
    movies = sorted(user_movies_test[user].keys())

    for movie in movies:
        prediction = 3
        if movie in movie_user_data:
            prediction = getPredictions(user, movie)
        else:
            num = 0
            denom = 0
            for mo in movie_user_data:
                if len(set(movie_generes_data[movie]).intersection(set(movie_generes_data[mo]))) > 0 :
                    if user in movie_user_data[mo]:
                        num += movie_user_data[mo][user]
                        denom += 1
              
            if denom != 0:
                prediction =  num / denom
                
        acc += (prediction - user_movies_test[user][movie]) ** 2
        
#         print user, movie, prediction, user_movies_test[user][movie]
        
        output_file.write("%s,%s,%s\n"%(user, movie, prediction))
        v = abs(prediction - user_movies_test[user][movie])
        
        if v >=0 and v <1:
            l[0]+=1
        elif v < 2:
            l[1]+=1
        elif v<3:
            l[2]+=1
        elif v<4:
            l[3]+=1
        else:
            l[4]+=1 
        
        
        n+=1
#         print  user, movie, prediction, user_movies_test[user][movie]
    
RMSE = (acc / n) ** 0.5

print ">=0 and <1:", l[0]
print ">=1 and <2:", l[1]
print ">=2 and <3:", l[2]
print ">=3 and <4:", l[3]
print ">=4:", l[4]
print "RMSE =", RMSE