from pyspark import SparkConf, SparkContext, SQLContext
import sys
import time

start = time.time()

conf = SparkConf().setAppName("Recommendation_System")
sc = SparkContext(conf = conf)
sqlcontext = SQLContext(sc)

train_file = sys.argv[1]
test_file = sys.argv[2]
 
train_data = sc.textFile(train_file)
header = train_data.first()
train_data = train_data.filter(lambda x : x!=header)
train_df = train_data.map(lambda x : (int(x.split(',')[0]), int(x.split(',')[1]), float(x.split(',')[2]))).toDF(['user', 'movie', 'rating'])

tf = train_df.select(train_df.user, train_df.movie)

test_data = sc.textFile(test_file)
header = test_data.first()
test_data = test_data.filter(lambda x : x!=header)
test_df = test_data.map(lambda x : (int(x.split(',')[0]), int(x.split(',')[1]))).toDF(['user', 'movie'])

new_train_df = tf.subtract(test_df)

joined_test_data = test_df.join(train_df, (test_df.movie == train_df.movie) & (test_df.user == train_df.user)).select(test_df.user, test_df.movie, train_df.rating)

joined_train_data = train_df.join(new_train_df, (new_train_df.movie.alias('m') == train_df.movie) & (new_train_df.user.alias('u') == train_df.user), 'inner').select(new_train_df.user.alias('user'), new_train_df.movie.alias('movie'), train_df.rating)

joined_train_data.coalesce(1).write.format('com.databricks.spark.csv').options(header='true').save('new_train_big.csv')
joined_test_data.coalesce(1).write.format('com.databricks.spark.csv').options(header='true').save('new_test_big.csv')