'''
Created on Oct 16, 2016

@author: chira
'''
import scipy.io as io
from svmutil import svm_train
from svmutil import svm_predict

phishing_data = io.loadmat("phishing-test.mat")

features = phishing_data['features']
labels = phishing_data['label'][0]


x = [list(f) for f in features]

y = list(labels)

m = svm_train(y, x ,'-t 2 -g 0.25 -c 16 ')

labels, acc, vals = svm_predict(y, x, m)

print acc