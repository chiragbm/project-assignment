'''
Created on Oct 10, 2016

@author: chira
'''


import math

from numpy import matrix as matrix
import numpy

import matplotlib.pyplot as pt
import matplotlib.pyplot as ptt
from msilib import Feature


def generate_data(size):
    
    x = numpy.random.uniform(-1, 1, size)
    epsilon = numpy.random.normal(0, 0.1, size)
    y = (2 * (x**2)) + epsilon
    return x, y


def create_dataset(t_size, size):
    
    data = []
    for k in range(t_size / size):
        x, y = generate_data(size)
        l = []
        for i in range(len(x)):
            l.append([1, x[i] , x[i] ** 2, x[i]** 3, x[i]**4, y[i]])
        data.append(l)
    return data
           
       
def get_weights(d_x, d_y):
    
    
    x = numpy.array(d_x)
    x_t = matrix.transpose(x)
    
    x_x_t = numpy.dot(x_t, x)
    y = numpy.array(d_y)
    x_y_t = numpy.dot(x_t, y)
    
    w = numpy.linalg.solve(x_x_t, x_y_t)
    
    return w

def get_mse(d_x, d_y, weights):
    
    err_sum = 0
 
    for index in range(len(d_x)):
        y_pred = 0
        for i in range(len(d_x[0])):
            y_pred += weights[i]*d_x[index][i]
         
        err_sum += (d_y[index] - y_pred)**2
    return err_sum / len(d_x)

def predict_y(data_x, weights):
    
    y_pred = 0
    for i in range(len(data_x)):
        y_pred += weights[i]*data_x[i]
    
    return y_pred

def calculate_bias_2(size, a_weights, x, y ):
    
    bias = 0.0
    weights = numpy.average(a_weights, axis = 0)
    
    j=0
    for v_x in x:
        sub_arr = []
        for i in range(size):
            sub_arr.append(v_x ** i)
        
        bias += (predict_y(sub_arr, weights) - y[j]) ** 2
        j+=1
    
    return round(bias / len(x),7)


def calculate_variance(size, a_weights, x):
    
    var = 0.0
    
    b_weights = numpy.average(a_weights, axis = 0)
    
    for v_x in x:
        sub_arr = []
        for i in range(size):
            sub_arr.append(v_x ** i)
            
            
            
        g_bar_x = predict_y(sub_arr,b_weights)
    
        sum = 0
        for weight in a_weights:
            sum += (predict_y(sub_arr, weight) - g_bar_x) ** 2
    
        var += sum / len(a_weights)
        
    return round(var / len(x), 7)

def get_ridge_weights(lam, d_x, d_y):
    
    iden = numpy.zeros((3,3), float)
    numpy.fill_diagonal(iden, lam )
    
    x = numpy.array(d_x)
    x_t = matrix.transpose(x)
    
    x_x_t = numpy.dot(x_t, x)
    x_x_t_i = numpy.add(x_x_t, iden)
    y = numpy.array(d_y)
    x_y_t = numpy.dot(x_t, y)
    
    w = numpy.linalg.solve(x_x_t_i, x_y_t)
    
    return w
    
        
######################### ---------------------- Starts Here --------------------- ###############################
# 
dataset = create_dataset(1000, 10)
d_x , d_y = generate_data(500)
 
hist_array = []
 
print 'Function\t\tBias\t\tVariance :'
 
sse = []
for d in dataset:
        y_sum = 0
        for l in d:
            y_sum += (l[5] - 1 )**2 
        mse = y_sum / len(d)
        sse.append(mse)
hist_array.append(sse)
# pt.hist(sse)
# pt.show()
 
bias = 0.0
     
j=0
for v_x in d_x:
     
    bias += (1 - d_y[j]) ** 2
    j+=1
 
bias =  bias / len(d_x)
 
print 'G(%s)\t\t%s\t\t%s'%(0,bias, 0)
 
 
for i in range(1,6):
    sse = []
    all_weights = []
    for d in dataset:
        x = []
        y = []
        for l in d:
            x.append(l[:i])
            y.append(l[5])
        w = get_weights(x, y)
        all_weights.append(w)
        mse = get_mse(x, y, w)
        sse.append(mse)
     
    print 'G(%s)\t\t%s\t\t%s'%(i,calculate_bias_2(i, all_weights, d_x, d_y), calculate_variance(i, all_weights, d_x))
    hist_array.append(sse)
#     pt.hist(sse)
#     pt.show()
#     pt.show()
 
print


attributes = ['g(0)','g(1)','g(2)','g(3)','g(4)','g(5)']

fig =pt.figure()
fig.subplots_adjust(left=0.125, bottom=0.1, right=0.9, top=0.9, wspace=0.4, hspace=0.4)
fig.suptitle("For Datasets of size 10", fontsize = 20)
# pt.title("Histogram for all features")
for i in range(6):

    fig.add_subplot(2, 3, i+1).hist(hist_array[i])
#     fig.add_subplot(5, 3, i+1).set_xlabel('Values')
    fig.add_subplot(2, 3, i+1).set_ylabel('Frequency')
    fig.add_subplot(2, 3, i+1).set_title(attributes[i])
    
pt.show()
     
#############################--------------------------- for 100 samples ---------------#####################    
dataset = create_dataset(10000, 100)
d_x , d_y = generate_data(500)

hist_array = [] 
 
print 'Function\t\tBias\t\tVariance :'
 
sse = []
for d in dataset:
        y_sum = 0
        for l in d:
            y_sum += (l[5] - 1 )**2 
        mse = y_sum / len(d)
        sse.append(mse)
hist_array.append(sse)
# ptt.hist(sse)
# ptt.show()
 
bias = 0.0
     
j=0
for v_x in d_x:
     
    bias += (1 - d_y[j]) ** 2
    j+=1
 
bias =  bias / len(d_x)
 
print 'G(%s)\t\t%s\t\t%s'%(0,bias, 0)
 
 
for i in range(1,6):
    sse = []
    all_weights = []
    for d in dataset:
        x = []
        y = []
        for l in d:
            x.append(l[:i])
            y.append(l[5])
        w = get_weights(x, y)
        all_weights.append(w)
        mse = get_mse(x, y, w)
        sse.append(mse)
    print 'G(%s)\t\t%s\t\t%s'%(i,calculate_bias_2(i, all_weights, d_x, d_y), calculate_variance(i, all_weights, d_x))
    hist_array.append(sse)
#     ptt.hist(sse)
#     ptt.show()


attributes = ['g(0)','g(1)','g(2)','g(3)','g(4)','g(5)']

fig =pt.figure()
fig.subplots_adjust(left=0.125, bottom=0.1, right=0.9, top=0.9, wspace=0.4, hspace=0.4)
fig.suptitle("For Datasets of size 100", fontsize = 20)
for i in range(6):

    fig.add_subplot(2, 3, i+1).hist(hist_array[i])
#     fig.add_subplot(5, 3, i+1).set_xlabel('Values')
    fig.add_subplot(2, 3, i+1).set_ylabel('Frequency')
    fig.add_subplot(2, 3, i+1).set_title(attributes[i])
    
pt.show()


print 
print 'Lambda\t\tBias\t\tVariance :'    
lambdas = [0.001, 0.003, 0.01, 0.03, 0.1, 0.3, 1.0]
 
for lamda in lambdas:
     
    all_weights = []
    for d in dataset:
        x = []
        y = []
        for l in d:
            x.append(l[:3])
            y.append(l[5])
        w = get_ridge_weights(lamda, x, y)
        all_weights.append(w)
#         mse = get_mse(x, y, w)
    print '%s\t\t%s\t\t%s'%(lamda,calculate_bias_2(3, all_weights, d_x, d_y), calculate_variance(3, all_weights, d_x))
#     ptt.hist(sse)
#     ptt.show()



import scipy.io as io
 
phishing_data = io.loadmat("phishing-train.mat")
 
features = phishing_data['features']
labels = phishing_data['label'][0]
 
features_transpose = numpy.transpose(features)
exp_cols = [1, 6, 7, 13, 14, 25, 28]
col = 18
 
for i in range(30):
    if i in exp_cols:
        ones=[]
        zeros=[]
         
        for j in range(2000):
             
            minus = 0
            one = 0
            zero = 0
             
            if features_transpose[i][j] == -1:
                minus = 1
            elif features_transpose[i][j] == 0:
                zero = 1
            else:
                one = 1
             
            features_transpose[i][j] = minus
            ones.append(one)
            zeros.append(zero)
         
        features_transpose = numpy.append(features_transpose, [ones], axis = 0)
        features_transpose = numpy.append(features_transpose, [zeros], axis = 0)
         
         
    elif i != col:
        for j in range(2000):
            if features_transpose[i][j] == -1:
                features_transpose[i][j] = 0
                 
                 
features_transpose = numpy.transpose(features_transpose)
 
x = [list(f) for f in features_transpose]
 
y = list(labels)


######################## ---- Normal values------####################

from svmutil import svm_train
import time
 
c_values = [4 ** -6,4 ** -5,4 ** -4,4 ** -3,4 ** -2,4 ** -1,1,4, 4 ** 2]
datalist = []
  
for c in c_values:
    start = time.time()
    acc = svm_train(y, x ,'-c %s -v 3'%c)
    end = time.time()
    t = round((end- start) / 3, 3)
    datalist.append([c, t, acc ])
          
print 
print "-------------------------------- Part - a -------------------------------"
print
  
print "c\t\tTime\t\tAccuracy"
  
for d in datalist:
    print round(d[0], 4),'\t\t',round(d[1],3),'\t\t',d[2]  
 
     
######################### ---------Polynomial Kernel ----------##################
#  
c_values = [4 ** -3,4 ** -2,4 ** -1,1,4 ** 1,4 ** 2,4 ** 3,4 ** 4,4 ** 5,4 ** 6,4 ** 7 ]
d_values = [1,2,3]
   
acc_list = []
avg_time_list = []  
 
max_p_acc = 0
max_p_c = 0
max_p_g = 0
   
datalist = []
   
for c in c_values:
    a = []
    avg = []
    for d in d_values:
        start = time.time()
        acc = svm_train(y, x ,'-t 1 -d %s -c %s -h 0 -v 3'%(d, c))
        end = time.time()
        if max_p_acc < acc:
            max_p_acc = acc
            max_p_c = c
            max_p_g = d
        t = round((end- start) / 3, 3)
        datalist.append([c,d, t, acc ])
           
print 
print "-------------------------------- Part - b -------------------------------"
print
   
print "c\t\t","degree\t\t","Time\t\t","Accuracy"
   
for d in datalist:
    print round(d[0], 4),'\t\t',round(d[1],3),'\t\t',d[2],'\t\t',d[3]  
 
 
################### ---- RBF ---------########################33
# 
c_values = [4 ** -3,4 ** -2,4 ** -1,1,4 ** 1,4 ** 2,4 ** 3,4 ** 4,4 ** 5,4 ** 6,4 ** 7 ]
d_values = [4 ** -7,4 ** -6,4 ** -5,4 ** -4,4 ** -3,4 ** -2,4 ** -1,1,4 ** 1, 4 ** 2]
  
acc_list = []
avg_time_list = []  
  
datalist = []
 
max_acc = 0
max_c = 0
max_g = 0
  
for c in c_values:
    a = []
    avg = []
    for d in d_values:
        start = time.time()
        acc = svm_train(y, x ,'-t 2 -g %s -c %s -h 0 -v 3'%(d, c))
        end = time.time()
        if max_acc < acc:
            max_acc = acc
            max_c = c
            max_g = d
         
        t = round((end- start) / 3, 3)
        datalist.append([c,d, t, acc ])
          
print 
print "-------------------------------- Part - c -------------------------------"
print
  
print "c\t\tgamma\t\tTime\t\tAccuracy"
  
for d in datalist:
    print round(d[0], 4),'\t\t',round(d[1],3),'\t\t',d[2],'\t\t',d[3]  
 
print
 
print "Best is for RBF : ", max_acc, max_c, max_g

