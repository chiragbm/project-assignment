'''
Created on Sep 17, 2016

@author: chira
'''
import numpy as nm
import heapq
import math


def get_mean_NB():
    
    class_features_array  = {}
    
    for idd in glass_data.keys():
        
      
        cl = glass_data[idd][9]
        if cl in class_features_array.keys():
            class_features_array[cl].append(glass_data[idd][0:9])
        else:
            l = []
            l.append(glass_data[idd][0:9])
            class_features_array[cl] = l
    
#     print class_features_array
          
    mean_class = {}
    std_class = {}
      
    for id in class_features_array.keys():
        
        mean_class[id] = nm.mean(class_features_array[id], axis=0)
        std_class[id] = nm.std(class_features_array[id], axis=0)
    
#     for id in class_features_array.keys():
# #         print id,mean_class[id]
#         print std_class[id]
#     mean_arr = nm.mean(value_arr, axis=0)
#     std_arr = nm.std(value_arr, axis=0,ddof=1)
#
    
#     for i in range(0,7):
#         print class_features_array[i]
#     print list
    
    
    return mean_class, std_class



def get_mean():
    value_arr = []
    
    for idd in glass_data.keys():
        value_arr.append(glass_data[idd][0:9])
    
    mean_arr = nm.mean(value_arr, axis=0)
    std_arr = nm.std(value_arr, axis=0,ddof=1)
    
    return mean_arr, std_arr


def normalize():
    
    for idd in glass_data.keys():
        for ind in range(0,9):
            glass_data[idd][ind] = (glass_data[idd][ind] - mean_arr[ind]) / std_arr[ind]
            
def normalize_dev_data():
    
    for idd in dev_data.keys():
        for ind in range(0,9):
            dev_data[idd][ind] = (dev_data[idd][ind] - mean_arr[ind]) / std_arr[ind]
        
 
def get_dist_l2(ar1, ar2):
    return nm.linalg.norm(ar1-ar2)

def get_dist_l1(ar1, ar2):
    return sum(abs(a-b) for a, b in zip(ar1, ar2))

def get_attr_array(idd):
    return glass_data[idd][0:9]

def get_dev_attr_array(idd):
    return dev_data[idd][0:9] 

def percentage(val):
    return round(float(val) * 100 / len(glass_data.keys()), 2)

def percentage_dev(val):
    return round(float(val) * 100 / len(dev_data.keys()), 2)

def max_label(heap):
    
    label_dict = {}
    
    for l in heap:
        label = l[1]
        val = l[0]
        if label_dict.get(label) != None:
            
            label_dict[label][0]+=1
            label_dict[label][1] = min(label_dict[label][1], val)
        else:
            li = []
            li.append(1)
            li.append(val)
            label_dict[label] = li
    
    #print label_dict
        
    max_count = -1
    label = None
    min_d = None
    
    for key in label_dict.keys():
        
        if label_dict[key][0] > max_count:
            label = key
            min_d = label_dict[key][1]
            max_count = label_dict[key][0]
        
        elif label_dict[key][0] == max_count:
            if label_dict[key][1] < min_d:
                    min_d = label_dict[key][1]
                    label = key
                
    return label
            
    

def process_train_data(k):
            
    accuracy_l1 = 0
    error_l1 = 0
    
    accuracy_l2 = 0
    error_l2 = 0
    
    for idd in glass_data.keys():
        heap_l1 = []
        heap_l2 = []
        att_ar = nm.array(get_attr_array(idd))
        for id in glass_data.keys():
            if id == idd:
                continue
            
            i_att_ar = nm.array(get_attr_array(id))
            
            d_l1 = get_dist_l1(att_ar, i_att_ar)
            d_l2 = get_dist_l2(att_ar, i_att_ar)
            
            heap_l1.append([d_l1, glass_data[id][9]])
            heap_l2.append([d_l2, glass_data[id][9]])
        
        label_l1 = max_label(heapq.nsmallest(k, heap_l1))
        label_l2 = max_label(heapq.nsmallest(k, heap_l2))
        
        if label_l1 == glass_data[idd][9]:
            accuracy_l1+=1
        else:
            error_l1+=1
            
        if label_l2 == glass_data[idd][9]:
            accuracy_l2+=1
        else:
            error_l2+=1
    
    #print 'k :', k
    #print 'L1 :','\t',accuracy_l1,'\t\t', error_l1
    #print 'L2 :','\t',accuracy_l2,'\t\t', error_l2
    print 'K :', k
    print 'Metric','\t','Accuracy','\t','Error'
    print 'L1 :','\t',percentage(accuracy_l1),'\t\t', percentage(error_l1)
    print 'L2 :','\t',percentage(accuracy_l2),'\t\t', percentage(error_l2)

def process_dev_data(k):
    accuracy_l1 = 0
    error_l1 = 0
    
    accuracy_l2 = 0
    error_l2 = 0
    
    for idd in dev_data.keys():
        heap_l1 = []
        heap_l2 = []
        att_ar = nm.array(get_dev_attr_array(idd))
        for id in glass_data.keys():

            i_att_ar = nm.array(get_attr_array(id))
            
            d_l1 = get_dist_l1(att_ar, i_att_ar)
            d_l2 = get_dist_l2(att_ar, i_att_ar)
            
            heap_l1.append([d_l1, glass_data[id][9]])
            heap_l2.append([d_l2, glass_data[id][9]])
        
        label_l1 = max_label(heapq.nsmallest(k, heap_l1))
        #print(label_l1, dev_data[idd][9])
        label_l2 = max_label(heapq.nsmallest(k, heap_l2))
        #print(label_l2, dev_data[idd][9])
        #print(label_l1, dev_data[idd][9])
        
        if label_l1 == dev_data[idd][9]:
            accuracy_l1+=1
        else:
            error_l1+=1
             
        if label_l2 == dev_data[idd][9]:
            accuracy_l2+=1
        else:
            error_l2+=1
    
#     print 'k :', k
#     print 'L1 :','\t',accuracy_l1,'\t\t', error_l1
#     print 'L2 :','\t',accuracy_l2,'\t\t', error_l2
    print 'K :', k
    print 'Metric','\t','Accuracy','\t','Error'
    print 'L1 :','\t',percentage_dev(accuracy_l1),'\t\t', percentage_dev(error_l1)
    print 'L2 :','\t',percentage_dev(accuracy_l2),'\t\t', percentage_dev(error_l2)

    
def pdf(x, mean, sd):
    
    var = float(sd)**2
    pi = math.pi
    denom = (2*pi*var)**.5
    num = math.exp(-(float(x)-float(mean))**2/(2*var))
    return float(num/denom)


def getCondProb(idd, cl):
    
    sum = 1
    for i in range(0,9):
        
        if std_class[cl][i] == 0:
            if mean_class[cl][i] == glass_data[idd][i]:
                sum = sum*1
            else:
                sum = sum*0
                return sum
        else:
            sum *= pdf(glass_data[idd][i],mean_class[cl][i] ,std_class[cl][i])
        
        
        
#         sum *= pdf(glass_data[idd][i],mean_class[cl][i] ,std_class[cl][i])
        #sum += math.log(pdf(glass_data[idd][i],mean_arr[i] ,std_arr[i]))
    return sum

def getCondProb_dev(idd,cl):
    
    sum = 1
    for i in range(0,9):
        
        if std_class[cl][i] == 0:
            if mean_class[cl][i] == dev_data[idd][i]:
                sum = sum*1
            else:
                sum = sum*0
                return sum
        else:
            sum *= pdf(dev_data[idd][i],mean_class[cl][i] ,std_class[cl][i])
        #sum += math.log(pdf(glass_data[idd][i],mean_arr[i] ,std_arr[i]))
    return sum

def NB_train():
    
    accuracy = 0
    error = 0
    
    for idd in glass_data.keys():
        val = None
        assign_label = None
        for cl in range(1,8):
            if cl == 4:
                continue
            p = getCondProb(idd, cl)
            v = (float(cl_count[cl-1]) / len(glass_data.keys())) * p #math.log(float(cl_count[cl-1]) / len(glass_data.keys())) + p
            
            if val != None:
                if(v > val):
                    val = v
                    assign_label = cl
            
            else:
                val = v
                assign_label = cl
        
        if glass_data[idd][9] == assign_label:
            accuracy+=1
        else:
            error+=1
    
    
    
    print 'Accuracy : ',percentage(accuracy)
    print 'Error    : ', percentage(error)    
    


def NB_test():
    
    accuracy = 0
    error = 0
    
    for idd in dev_data.keys():
        val = None
        assign_label = None
        for cl in range(1,8):
            if cl == 4:
                continue
            p = getCondProb_dev(idd, cl)
            v = (float(cl_count[cl-1]) / len(glass_data.keys())) * p #math.log(float(cl_count[cl-1]) / len(glass_data.keys())) + p
            
            if val != None:
                if(v > val):
                    val = v
                    assign_label = cl
            
            else:
                val = v
                assign_label = cl
        
        if dev_data[idd][9] == assign_label:
            accuracy+=1
        else:
            error+=1
    
    print 'Accuracy : ',percentage_dev(accuracy)
    print 'Error    : ', percentage_dev(error) 
                           
'''
-----------------------------------NB - Test ----------------------------------------------------------
-----------------------------------Start Here ---------------------------------------------------
''' 
cl_count = [0,0,0,0,0,0,0]  
glass_file  = open('train.txt','r')
glass_data = {}

while(True):
    
    line = glass_file.readline()
    
    if(line==""):
        break
    line = line.strip()
    values = line.split(',')
    
    idd = int(values[0])
    val_arr = []
    for i in range(1, 10):
        val_arr.append(float(values[i]))
    
    val_arr.append(int(values[10]))
    cl_count[int(values[10]) - 1] +=1
    glass_data[idd] = val_arr

     
# Normalize data points
mean_class, std_class = get_mean_NB()

 
print '----------------Naive_Bayes-------------------'
print
print 'Training Data - Details'
print
NB_train()
 
 
dev_file = open('test.txt', 'r')
 
dev_data = {}
  
while(True):
     
    line = dev_file.readline()
     
    if(line==""):
        break
    line = line.strip()
    values = line.split(',')
     
    idd = int(values[0])
    val_arr = []
    for i in range(1, 10):
        val_arr.append(float(values[i]))
     
    val_arr.append(int(values[10]))
    dev_data[idd] = val_arr
 
print    
print 'Test Data - Details'
print
NB_test()
 
 
'''
-----------------------------------KNN ----------------------------------------------------------
-----------------------------------Start Here ---------------------------------------------------
''' 
 
 
mean_arr, std_arr = get_mean()
 
# print mean_arr
# print std_arr 
normalize()
  
print
print '--------------------KNN----------------------'
print
print "Training Details: "
print
process_train_data(1)
print
process_train_data(3)
print
process_train_data(5)
print
process_train_data(7)
              
  
'''
 -----------------------------------------Test Data -----------------------------------------------------
'''
  
normalize_dev_data()
  
# print(dev_data[146])
  
print
print 
print "Test Details: "
print
process_dev_data(1)
print
process_dev_data(3)
print
process_dev_data(5)
print
process_dev_data(7)
  


