'''
Created on Jul 19, 2016

@author: Chiragbm
'''
import sys
import re


def first(l):
    return l[0]

def rest(l):
    new = list()
    new.extend(l[1:])
    return new
    
def probability(var, bool_val, byn, ev):
    
    pars = byn[var][0]
    true_value = 0.0
    if(len(pars)<=0):
        true_value = byn[var][1][None]
    else:
        parsVal = list()
        for p in pars:
            if(ev[p]):
                parsVal.append('+')
            else:
                parsVal.append('-')
        
        true_value = byn[var][1][tuple(parsVal)]
        
    if(bool_val):
        return true_value
    else:
        return 1.0 - true_value


def enumerateAll(parents, ev):
    if len(parents)==0:
        return 1.0
    
    Y = first(parents)
    
    if Y in ev.keys():
        v = probability(Y, ev[Y], byn, ev)
        v = v * enumerateAll(rest(parents), ev)
        return v
    else:
        total = 0.0
        ev[Y] = True
        vp = probability(Y, True, byn, ev)
        vp = vp * enumerateAll(rest(parents), ev)
        total = total + vp
        del ev[Y]
        ev[Y] = False
        vf = probability(Y, False, byn, ev)
        vf = vf * enumerateAll(rest(parents), ev)
        total = total + vf
        del ev[Y]
        return total
    
def normalize(QX):

    total = 0.0
    total = total + QX[False] + QX[True]
    QX[False] = QX[False]/total
    QX[True] = QX[True]/total

    return QX    

def enumerationAsk(que, ev, byn, parents):

    if len(que)>0:
        QX=[False, True]
        QXDict={}
        for x in QX:
            ev[que] = x
            QXDict[x] = enumerateAll(parents, ev)
            del ev[que]
        return normalize(QXDict)
    else:
        return enumerateAll(parents, ev)
        



#########-----------------------------Starts Here ---------------------------------------

inp = open(sys.argv[1],'r')
out = open("output.txt",'w+')
qlen = int(inp.readline())
queries = list()
parentsVar = list()
askQueries = list()
"""
    Get all queries
"""
for q in range(0,qlen):
    queries.append(inp.readline().strip())

"""
    Prepare Bayes-Net
"""
pattern = r'[a-zA-Z]*'
byn = {}
while(True):
    line = inp.readline()
    if(line==""):
        break
    
    line = line.strip()
    if(line=="***"):
        continue
    
    
    match = re.match(pattern, line)
    if(len(match.group(0))>0):
        parts = line.partition('|')
        parent = parts[0].strip()
        parentsVar.append(parent)
        childs = parts[2].strip()
        valueList = list()       # Value list for main dictionary
        if len(childs) > 0:
            children = childs.split(" ")
            valueList.append(children)
            clen = len(children)
            clen = int(pow(2, clen))
            cDict={}                                        # Child dictionary
            for i in range(0,clen):
                line = (inp.readline()).strip()
                values = line.split(" ")                    # values for each row of child
                vlen = len(values)                          # len of values
                tup = list()                                # tuple - key of dictionary
                value = float(values[0])                    # probability value
                
                if(vlen<=2):
                    val = values[1] 
                    tup.append(val)                
                    t = tuple(tup)
                    cDict[t]=value
                else:
                    for j in range(0,vlen-1):                        
                            tup.append(values[j+1])
                t = tuple(tup)
                cDict[t] = value
                    
            valueList.append(cDict)
            byn[parent] = valueList
            
        else:
            valueList.append([])
            prob = float(inp.readline().strip())
            cDict = {}
            cDict[None]=prob
            valueList.append(cDict)
            byn[parent] = valueList
            


'''
    update query format
'''
for query in queries:
    start_comma = query.index('(')
    end_comma = query.find(')')
    query = query[start_comma+1: end_comma]
    q_list = query.partition('|')
    ask = ""
    evidences = ''
    evDict = {}
    if len(q_list[2]) > 0 :
        ask = q_list[0].strip()
        evidences = q_list[2].strip()
    else:
        evidences = q_list[0].strip()
    
    #for evidence in evidences:
    evList = evidences.split(",")
    for ev in evList:
        key_value = ev.split("=")
        key = key_value[0].strip()
        value = key_value[1].strip()
        if(value =="+"):
            evDict[key] = True
        else:
            evDict[key] = False
            
    l = list()
    l.append(ask)
    l.append(evDict)
    
    askQueries.append(l)    

new_line = False
for query in askQueries:
    ask = query[0]
    evidence = query[1]
    final_value = 0.0
    if(len(ask)>0):
        ask_List = ask.split("=")
        ask_key = ask_List[0].strip()
        ask_value = ask_List[1].strip()
        ansDict = enumerationAsk(ask_key, evidence, byn, parentsVar)
        print(ansDict)
        if(ask_value=='+'):
            final_value = format(ansDict[True],'.2f') 
        else:
            final_value = format(ansDict[False],'.2f')
    else:
        final_value = format(enumerationAsk([], evidence, byn, parentsVar),'.2f')
    
    if(new_line):
        out.write('\n')
    else:
        new_line = True
    out.write(final_value)


            
        
        
    
    
    