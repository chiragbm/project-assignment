'''
Created on Nov 25, 2016

@author: chira
'''
import sys
import os
from hw3_corpus_tool import get_data

dir = sys.argv[1]
outputFile = open(sys.argv[2], 'r')

test_gen_obj = get_data(dir)

a = 0
e = 0


while True:
    line = outputFile.readline().strip()
    if line != '':
        fileargs = line.split('=')
        filename = dir+'\\'+fileargs[1][1:-1]
        fopen = open(filename)
        utterances = test_gen_obj.next()[1]
        iter = 0
        while True:
            line = outputFile.readline().strip()
            if line != '':
                if utterances[iter].act_tag == line:
                    a+=1
                else:
                    e+=1
                iter+=1
            else:
                break
#         count+=1
    else:
        break
 
print a, e, (float(a)*100)/(a+e)