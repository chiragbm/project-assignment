'''
Created on Nov 17, 2016

@author: chira
'''

import sys
import os
import time
import pycrfsuite

from hw3_corpus_tool import get_data


false = '0'
true = '1'
sv_words = ['you','certainly','seems', 'seemed','think','believe','suppose','might','view','sure','certain','convinced ','opinions', 'opinion','claimed','deny','imagine','say','claim','admit','convince']
def get_label_feature_for_utterances(utterances):
    
    labels = []
    features = []
    
    for i in range(len(utterances)):
        feature = []
        
        if i != 0:
            ## This logic is for checking speaker changed or not.
            if utterances[i-1].speaker == utterances[i].speaker:
                feature.append(false)
            else:
                feature.append(true)
            
            feature.append(false)
        else:
            
            feature.append(false)
            feature.append(true)
        
        postag = utterances[i].pos
        
        
        if i > 0 and i< len(utterances)-1:
            if utterances[i-1].pos != None:
                feature.append('PREV_UTTER_LAST_TOKEN'+utterances[i-1].pos[len(utterances[i-1].pos) - 1].token)
                feature.append('NEXT_UTTER_FIRST_TOKEN'+utterances[i-1].pos[0].token)
#                 feature.append('PREV_UTTER_LAST_POS'+utterances[i-1].pos[len(utterances[i-1].pos) - 1].pos)
        
        if postag == None:
            postag = []
        else:
            feature.append("FIRST_TOKEN_"+postag[0].token)
            feature.append("LAST_TOKEN_"+postag[-1].token)
#             feature.append("LAST_POS_"+postag[-1].pos)
            
        
        for j in range(len(postag)):
            tkn = postag[j].token
            ps = postag[j].pos
            
            feature.append('TOKEN_'+tkn)
            feature.append('LOWER_'+tkn.lower())
            feature.append('POS_'+ps)
        
        labels.append(utterances[i].act_tag)
        
        features.append(feature)
        
    return labels, features

def get_labels_features_list(genObj):
    
    labels = []
    features = []
    
    for utterances in genObj:
        
        utterances = utterances[1]
    
        label, feature = get_label_feature_for_utterances(utterances)
        labels += label
        features+= feature 
        
    return labels, features

start = time.time()

inputDir = sys.argv[1]
testDir = sys.argv[2]
    
'''
    Reading Training Data
    
'''

trainGenObj  = get_data(inputDir)    
train_labels, train_features = get_labels_features_list(trainGenObj)

'''
    Training Data 
    
'''
trainer = pycrfsuite.Trainer(algorithm = 'lbfgs',verbose = False)
trainer.append(train_features, train_labels)
trainer.set_params({
    'c1': 1.1,   # coefficient for L1 penalty
    'c2': 1e-6,  # coefficient for L2 penalty
    'max_iterations': 110,  # stop earlier
    # include transitions that are possible, but not observed
    'feature.possible_transitions': True})
trainer.train('model.crf')
tagger = pycrfsuite.Tagger()
tagger.open('model.crf')
 
 
'''
    Testing data

'''
outputFile = open(sys.argv[3], 'w')

testGenObj  = get_data(testDir)

y_predicted = []
y_orig = []
for test_utterances in testGenObj:
        
        test_utterance = test_utterances[1]
    
        label, feature = get_label_feature_for_utterances(test_utterance)
        
        y_orig+=label
        y_pred = tagger.tag(pycrfsuite.ItemSequence(feature))
        y_predicted+=y_pred
        outputFile.write('Filename="%s"\n'%(os.path.basename(test_utterances[0])))
        for y in y_pred:
            outputFile.write('%s\n'%y)
        outputFile.write('\n')

print(time.time() - start)

print len(y_predicted)
print len(y_orig)
 
a=0
e=0
 
for i in range(len(y_orig)):
    if y_orig[i] == y_predicted[i]:
        a+=1
    else:
        e+=1

print a, e, (float(a)*100)/(a+e)