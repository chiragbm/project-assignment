'''
Created on Nov 17, 2016

@author: chira
'''

import sys
import os
import time
import pycrfsuite

from hw3_corpus_tool import get_data


false = '0'
true = '1'

def get_label_feature_for_utterances(utterances):
    
    labels = []
    features = []
    
    for i in range(len(utterances)):
        feature = []
        
        if i != 0:
            ## This logic is for checking speaker changed or not.
            if utterances[i-1].speaker == utterances[i].speaker:
                feature.append(false)
            else:
                feature.append(true)
            
            feature.append(false)
        else:
            
            feature.append(false)
            feature.append(true)
        
        postag = utterances[i].pos
        
        if postag == None:
            postag = []
            
        
        for word_tags_tuple in postag:   
            feature.append('TOKEN_'+word_tags_tuple.token)
            feature.append('POS_'+word_tags_tuple.pos)
        
        labels.append(utterances[i].act_tag)
        
        features.append(feature)
        
    return labels, features

def get_labels_features_list(genObj):
    
    labels = []
    features = []
    
    for utterances in genObj:
        
        utterances = utterances[1]
    
        label, feature = get_label_feature_for_utterances(utterances)
        labels.extend(label)
        features.extend(feature)
        
    return labels, features


start = time.time()

inputDir = sys.argv[1]
testDir = sys.argv[2]

'''
    Reading Training Data
    
'''

trainGenObj  = get_data(inputDir)    
train_labels, train_features = get_labels_features_list(trainGenObj)

'''
    
    Training Data
    
'''


trainer = pycrfsuite.Trainer(algorithm = 'lbfgs',verbose = False)
trainer.append(train_features, train_labels)
trainer.set_params({
    'c1': 1.5,   # coefficient for L1 penalty
    'c2': 1e-6,  # coefficient for L2 penalty
    'max_iterations': 110,  # stop earlier
    # include transitions that are possible, but not observed
    'feature.possible_transitions': True})

trainer.train('model.crf')
tagger = pycrfsuite.Tagger()
tagger.open('model.crf')
 
 
'''
    Testing data

'''
outputFile = open(sys.argv[3], 'w')

testGenObj  = get_data(testDir)

for test_utterances in testGenObj:
        
        test_utterance = test_utterances[1]
    
        label, feature = get_label_feature_for_utterances(test_utterance)
        y_pred = tagger.tag(pycrfsuite.ItemSequence(feature))
        outputFile.write('Filename="%s"\n'%(os.path.basename(test_utterances[0])))
        for y in y_pred:
            outputFile.write('%s\n'%y)
        outputFile.write('\n')

outputFile.close()

print(time.time()-start)
