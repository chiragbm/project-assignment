'''
Created on Oct 8, 2016

@author: chira
'''


import sys
import os
from _collections import defaultdict


def process_file(filename):
    
    global bias
    
    with open(filename, 'r', encoding = 'latin1') as f:
        alpha = 0
        for line in f:
            words = line.split()
            for word in words:
                alpha += weights[word]
                    
        alpha += bias
        
        return alpha
    f.close()


def calculate_precision(correct_classified, total_classified):
    return correct_classified / total_classified

def calculate_recall(correct_classified, totat_belongs_class):
    return correct_classified / totat_belongs_class

def calculate_f1(correct_classified, total_classified, totat_belongs_class):
    
    prec = calculate_precision(correct_classified, total_classified)
    recall = calculate_recall(correct_classified, totat_belongs_class)
    
    f1 = (2 * prec * recall) / (prec + recall)
    
    return f1

def calculate_wa(f1_spam, f1_ham, no_of_spam, no_of_ham):
    
    return ((no_of_spam * f1_spam) + (no_of_ham * f1_ham)) / (no_of_ham + no_of_spam)

##############--------------------- Start Here ------------------------###################

path = sys.argv[1]
f = open('per_model.txt', 'r', encoding = 'latin1')
bias = float(f.readline())


weights = defaultdict(lambda : 0)

while(True):
    line = f.readline()
     
    if line == '':
        break
    line = line.strip()
    key_value = line.split(" ")
    weights[key_value[0]] = float(key_value[1])

real_spam_file_count = 0
real_ham_file_count = 0
correct_classified_spam = 0
correct_classified_ham = 0
file_classified_ham = 0
file_classified_spam = 0
is_spam = False

spam = None
ham = None

out = open(sys.argv[2], 'w', encoding = 'latin1')

for directory, subdirectories, files in os.walk(path):
 
    for file in files:
        if file.endswith('.txt'):
            filename = os.path.join(directory, file)
            value = process_file(filename)
            if(value > 0):
                spam = 1
                ham = 0
#                 print('spam')
            else:
                spam = 0
                ham = 1
                
#             if spam != 'ignore' and ham != 'ignore':
            if(spam > ham):
                is_spam = True
                file_classified_spam +=1
                out.write("spam %s\n"%(filename))
            else:
                is_spam = False
                file_classified_ham +=1
                out.write("ham %s\n"%(filename))
        
#             if directory.endswith('ham'):
#                 
#                 if not is_spam:
#                     correct_classified_ham+=1
#                 real_ham_file_count +=1
#                 
#                 
#             elif directory.endswith('spam'):
#                 
#                 if is_spam:
#                     correct_classified_spam+=1
#                 real_spam_file_count+=1
#             
            
 
out.close()

# if not (real_spam_file_count  == 0 or real_ham_file_count == 0 or correct_classified_spam == 0 or  correct_classified_ham==0 or file_classified_ham == 0 or file_classified_spam == 0):
# 
# #     print(real_spam_file_count)
# #     print(real_ham_file_count)
# #     print(correct_classified_spam)
# #     print(correct_classified_ham)
# #     print(file_classified_ham)
# #     print(file_classified_spam)
# #     print()
#     print('class\tprecision recall f1')
#     
#     f1_s = calculate_f1(correct_classified_spam, file_classified_spam, real_spam_file_count)
#     f1_h = calculate_f1(correct_classified_ham, file_classified_ham, real_ham_file_count)
#     
#     print('spam\t%.2f\t  %.2f\t %.2f'%(calculate_precision(correct_classified_spam, file_classified_spam),calculate_recall(correct_classified_spam, real_spam_file_count), f1_s))
#     print('ham\t%.2f\t  %.2f\t %.2f'%(calculate_precision(correct_classified_ham, file_classified_ham),calculate_recall(correct_classified_ham, real_ham_file_count), f1_h))
#     
#     wa = calculate_wa(f1_s, f1_h, real_spam_file_count, real_ham_file_count)
# 
#     print('WA :', round(wa,2))

