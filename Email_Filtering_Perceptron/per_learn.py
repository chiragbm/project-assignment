'''
Created on Oct 7, 2016

@author: chira
'''


import sys
import os
import random
import time
from _collections import defaultdict

def process_file(filename):
    
    global bias
    
    label = filename[0]
    name = filename[1]
    alpha = 0
    
    temp = []
    
    if not doc_dict.__contains__(name):
    
        with open(name, 'r', encoding = 'latin1') as f:
            for line in f:
                line = line.strip()
                words = line.split()
                for word in words:
                    alpha += weights[word]
                    temp.append(word)
                    
            alpha += bias
        
        f.close()
        doc_dict[name] = ' '.join(temp)
    
    else:
        line = doc_dict[name]
        words = line.split()
        
        for word in words:
            alpha += weights[word]
            temp.append(word)
                    
        alpha += bias
        
    
    if label*alpha <= 0:
        
        for word in temp:
            weights[word] += label
    
        bias += label
        
    temp.clear()
                    
            

def shuffle_and_processfiles(filename_array):
    
    random.shuffle(filename_array)
    
    for filename in filename_array:
        process_file(filename)

def create_small_array(start, end, ar, filess):
    
    for i in range(start, end):
        ar.append(filess[i])
    
    
    

#################------------------------Starts Here ---------------------########################

start_time = time.time()

path = sys.argv[1]
ham_file_paths = []
spam_file_paths = []
weights = defaultdict(lambda: 0)
doc_dict = {}
bias = 0
divide = 100


for directory, subdir, files in os.walk(path):
    
    if directory.endswith('ham'):
        for file in files:
            if file.endswith('.txt'):
                ham_file_paths.append([-1, os.path.join(directory, file)])
    elif directory.endswith('spam'):
        for file in files:
            if file.endswith('.txt'):
                spam_file_paths.append([1, os.path.join(directory, file)])


ham_len = len(ham_file_paths)
spam_len = len(spam_file_paths)

ratio = ham_len / spam_len
ratio = int(ratio*divide)

spam_index = divide
ham_index = ratio

times = int(spam_len / divide)

for _ in range(20):
    
    random.shuffle(ham_file_paths)
    random.shuffle(spam_file_paths)
    
    rem_spam = spam_len - (spam_index * times)
    rem_ham = ham_len - (ham_index * times)
    
    
    rem_files = list()
    create_small_array(0,rem_spam, rem_files, spam_file_paths)
    create_small_array(0,rem_ham, rem_files, ham_file_paths)
    
    
    
    shuffle_and_processfiles(rem_files)
    rem_files.clear()
    
    
    for i in range(times):
        arr = []
        
        create_small_array(rem_spam, (rem_spam + spam_index), arr, spam_file_paths)
        create_small_array(rem_ham,( rem_ham + ham_index), arr, ham_file_paths)
        
        
        shuffle_and_processfiles(arr)
        
        arr.clear()
        
        rem_spam += spam_index
        rem_ham += ham_index
    

o = open('per_model.txt','w', encoding = 'latin1')

o.write(str(bias)+'\n')
for i in weights:
    o.write(i+' '+str(weights[i])+'\n')

o.close()
print(time.time() - start_time )
        
'''

Take file from each group and also process remaining files

'''



