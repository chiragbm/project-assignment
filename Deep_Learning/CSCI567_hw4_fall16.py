'''
Created on Oct 27, 2016

@author: chira
'''
from hw_utils import loaddata, normalize, testmodels
import time


def d():
    
    arch = [[50,2],[50,10,2],[50,50,50,2],[50,50,50,50,2]]
    testmodels(X_tr, y_tr, X_te, y_te, arch, actfn='linear', last_act='softmax', reg_coeffs=[0.0], 
                num_epoch=30, batch_size=1000, sgd_lr=0.001, sgd_decays=[0.0], sgd_moms=[0.0], 
                    sgd_Nesterov=False, EStop=False, verbose=0)
    
    start = time.time()

    din = 50
    dout = 2
    arch = [[din, 50, dout], [din, 500, dout], [din, 500, 300, dout], [din, 800, 500, 300, dout], [din, 800, 800, 500, 300, dout]]
    testmodels(X_tr, y_tr, X_te, y_te, arch, actfn='linear', last_act='softmax', reg_coeffs=[0.0], 
                num_epoch=30, batch_size=1000, sgd_lr=0.001, sgd_decays=[0.0], sgd_moms=[0.0], 
                    sgd_Nesterov=False, EStop=False, verbose=0)
    
    end = time.time()
    print(end - start)

    
def e():
    
    din = 50
    dout = 2
    arch = [[din, 50, dout], [din, 500, dout], [din, 500, 300, dout], [din, 800, 500, 300, dout], [din, 800, 800, 500, 300, dout]]
    start = time.time()
    testmodels(X_tr, y_tr, X_te, y_te, arch, actfn='sigmoid', last_act='softmax', reg_coeffs=[0.0], 
                num_epoch=30, batch_size=1000, sgd_lr=0.001, sgd_decays=[0.0], sgd_moms=[0.0], 
                    sgd_Nesterov=False, EStop=False, verbose=0)
    end = time.time()
    print(end - start)

    
def f():
    din = 50
    dout = 2
    arch = [[din, 50, dout], [din, 500, dout], [din, 500, 300, dout], [din, 800, 500, 300, dout], [din, 800, 800, 500, 300, dout]]
    start = time.time()
    testmodels(X_tr, y_tr, X_te, y_te, arch, actfn='relu', last_act='softmax', reg_coeffs=[0.0], 
                num_epoch=30, batch_size=1000, sgd_lr=0.0005, sgd_decays=[0.0], sgd_moms=[0.0], 
                    sgd_Nesterov=False, EStop=False, verbose=0)
    end = time.time()
    print(end - start)
    
def g():  
    
    arch = [[50, 800, 500, 300, 2]]
    start = time.time()
    testmodels(X_tr, y_tr, X_te, y_te, arch, actfn='relu', last_act='softmax', reg_coeffs = [0.0000001, 0.0000005, 0.000001,0.000005, 0.00001], 
                num_epoch=30, batch_size=1000, sgd_lr=0.0005, sgd_decays=[0.0], sgd_moms=[0.0], 
                    sgd_Nesterov=False, EStop=False, verbose=0) 
    end = time.time()
    print(end - start) 

def h():
    
    arch = [[50, 800, 500, 300, 2]]
    start = time.time()
    testmodels(X_tr, y_tr, X_te, y_te, arch, actfn='relu', last_act='softmax', reg_coeffs = [0.0000001, 0.0000005, 0.000001,0.000005, 0.00001], 
                num_epoch=30, batch_size=1000, sgd_lr=0.0005, sgd_decays=[0.0], sgd_moms=[0.0], 
                    sgd_Nesterov=False, EStop=True, verbose=0)
    end = time.time()
    print(end - start)
    
def i():
    arch = [[50, 800, 500, 300, 2]]
    start = time.time()
    testmodels(X_tr, y_tr, X_te, y_te, arch, actfn='relu', last_act='softmax', reg_coeffs = [0.0000005], 
                num_epoch=100, batch_size=1000, sgd_lr=0.00001, sgd_decays= [0.00001, 0.00005,0.0001, 0.0003,0.0007, 0.001], sgd_moms=[0.0], 
                    sgd_Nesterov=False, EStop=False, verbose=0)
    end = time.time()
    print(end - start)

def j():
    arch = [[50, 800, 500, 300, 2]]
    start = time.time()
    testmodels(X_tr, y_tr, X_te, y_te, arch, actfn='relu', last_act='softmax', reg_coeffs = [0.0], 
                num_epoch=50, batch_size=1000, sgd_lr=0.00001, sgd_decays= [0.00001], sgd_moms=[0.99, 0.98, 0.95, 0.9,0.85],
                    sgd_Nesterov=True, EStop=False, verbose=0)
    end = time.time()
    print(end - start)
    
def k():
    arch = [[50, 800, 500, 300, 2]]
    start = time.time()
    testmodels(X_tr, y_tr, X_te, y_te, arch, actfn='relu', last_act='softmax', reg_coeffs = [0.00001], 
                num_epoch=100, batch_size=1000, sgd_lr=0.00001, sgd_decays= [0.00001], sgd_moms=[0.99],
                    sgd_Nesterov=True, EStop=True, verbose=0)
    end = time.time()
    print(end - start)
#     
def l():
    din = 50
    dout = 2
    arch = [[din, 50, dout], [din, 500, dout], [din, 500, 300, dout], [din, 800, 500, 300, dout], [din, 800, 800, 500, 300, dout]]
    start = time.time()
    testmodels(X_tr, y_tr, X_te, y_te, arch, actfn='relu', last_act='softmax', reg_coeffs = [0.0000001, 0.0000005, 0.000001,0.000005, 0.00001], 
                num_epoch=100, batch_size=1000, sgd_lr=0.00001, sgd_decays= [0.00001,0.00005,0.0001], sgd_moms=[0.99],
                    sgd_Nesterov=True, EStop=True, verbose=0)
    end = time.time()
    print(end - start)
    
#################### --------------------- Starts

X_tr,y_tr,X_te,y_te = loaddata('MiniBooNE_PID.txt')

X_tr, X_te =  normalize(X_tr, X_te)

d()
e()
f()
g()
h()
i()
j()
k()
l()


