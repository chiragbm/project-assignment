'''
Created on Mar 28, 2017

@author: chira
'''

from _collections import defaultdict
import itertools
from pyspark.conf import SparkConf
from pyspark.context import SparkContext
import sys
import time

def even(a, b, v):
    return (a * v + b) % 671

def odd(a, b, p, v):
    return ((a * v + b) % p) % 671

def map_hash_func(iterator):
    
    movies_hash_value = {}
    movies_hash_tuples = []
    
    for it in iterator:
        movie = it[0]
        user_list =  it[1]
        
        for user in user_list:
            for h_ind in range(0,100):
                
                a = h_ind + 5
                b = h_ind + 7
                val = None
                if(h_ind % 2 == 0):
                    val =  even(a, b, user)
                else:
                    val =  odd(a, b, prime_nos[h_ind % 40] ,user)
                
                if(movies_hash_value.has_key(movie)):
                    
                    if h_ind in movies_hash_value[movie]:
                        if(val < movies_hash_value[movie][h_ind]):
                            movies_hash_value[movie][h_ind] = val
                    else:
                        movies_hash_value[movie][h_ind] = val
                            
                else:
                    movies_hash_value[movie] = {}
                    movies_hash_value[movie][h_ind] = val
        
        t = (movie, [movies_hash_value[movie][i] for i in movies_hash_value[movie].keys()] )
        movies_hash_tuples.append(t)     
            
    return iter(movies_hash_tuples)

################################## --------------------------- Start ------------------------------###############################################
## Create Data
# start_time = time.time()
conf = SparkConf().setAppName("LSH")
sc = SparkContext(conf = conf)

file_name = sys.argv[1]
output_file = sys.argv[2]


data = sc.textFile(file_name)
header = data.first()
data = data.filter(lambda x : x!=header)

# true_data = sc.textFile('data\SimilarMovies.GroundTruth.05.csv')
# true_similar_movies = true_data.map(lambda x : (int(x.split(',')[0]), int(x.split(',')[1]))).collect()

bands = 25
rows = 4
prime_nos = [2,3,5,7,11,13,17,19,23,29,2,3,5,7,11,13,17,19,23,29,2,3,5,7,11,13,17,19,23,29,2,3,5,7,11,13,17,19,23,29,2,3,5,7,11,13,17,19,23,29]

movie_users = data.map(lambda x : (int(x.split(',')[1]), int(x.split(',')[0]))).groupByKey().mapValues(list)

data = movie_users.mapPartitions(map_hash_func).sortByKey().collect()

movies_users = movie_users.collectAsMap()
  
candidate_pairs = set()
candidates = []
 
for b in range(bands):
    band_dict = defaultdict(lambda : set())
    start = b * rows
    end = start + rows
    for tuple in data:
        s = '-'.join(str(e) for e in tuple[1][start: end])
        band_dict[s].add(tuple[0])
     
    for key in band_dict:
        if len(band_dict[key]) > 1:
            candidates.append(band_dict[key])
 
# print len(candidates)
 
for candidate in candidates:
    for pair in itertools.combinations(sorted(candidate), 2):
        candidate_pairs.add(pair)
 
# print len(candidate_pairs)

similar_pairs = []


for pair in candidate_pairs:
    similarity = float(len(set(movies_users[pair[0]]).intersection(set(movies_users[pair[1]])))) / len(set(movies_users[pair[0]]).union(set(movies_users[pair[1]])))
    if similarity >= 0.5:
        similar_pairs.append((pair[0], pair[1], similarity))

similar_pairs = sorted(similar_pairs, key = lambda t : (t[0],t[1]))

# print len(similar_pairs)
# print len(true_similar_movies)
# print len(set(similar_pairs).intersection(set(true_similar_movies)))
# print float(len(similar_pairs)) / len(true_similar_movies)

out = open(output_file, 'w')
for pair in similar_pairs:
    out.write("%s,%s,%s\n"%(pair[0],pair[1],pair[2]))

# print (time.time() - start_time)
            




