'''
Created on Nov 6, 2016

@author: chira
'''
import math
import random

from mpl_toolkits.mplot3d import Axes3D
import numpy
import pandas
from pandas.core.frame import DataFrame
from pandas.core.series import Series
from pip._vendor.pyparsing import alphas
from scipy.spatial.distance import euclidean
from scipy.stats import multivariate_normal as mn

import matplotlib.pyplot as plt
from sympy.integrals.risch import NonElementaryIntegral


def get_clusters(mu, data, size):
        
        cluster = [[] for _  in range(size)]
        for i in range(len(data['x'])):
            d=[]
            for cols in data.columns:
                d.append(data[cols][i])
            
            euc_dis = float('inf')
            label = 0
            for j in range(size):
                dist = euclidean(d, mu[j])
                if dist < euc_dis:
                    label = j
                    euc_dis = dist
            cluster[label].append(d)
                
        return cluster

def update_mu(cluster, size):
    
    mu = []
    for i in range(size):
        mu.append(list(numpy.mean(cluster[i], axis = 0)))
    return mu

def perform_KMeans(cluster_data, cluster_size):
    
#     plt.plot(cluster_data['x'],cluster_data['y'])
#     plt.show()
    
    clu = []
    
    for c_size in cluster_size:
        
        myu = []
        for i in range(c_size):
            r = int(numpy.random.randint(0, len(cluster_data['x'])))
            c = []
            for cols in cluster_data.columns:
                c.append(cluster_data[cols][r])
            myu.append(c)
        
        for k in range(100):
            clusters = get_clusters(myu, cluster_data, c_size)
            new_myu = update_mu(clusters, c_size)
            
            same = True

            if myu == new_myu:
                same = True
            else:
                same = False
            
            if same:
                print 'Seperated',k
                print myu
                print new_myu
                
                clu = clusters
                
                for j in range(c_size):
                    plt.plot(numpy.array(clu[j]).transpose()[0],numpy.array(clu[j]).transpose()[1], '.')
                plt.show()
                            
                break
            else:
                myu = new_myu

def perform_Kernel_KMeans(cluster_data):
    
    cluster_size = [2]
    clu = []
    
    for c_size in cluster_size:
        
        myu = []
        for i in range(c_size):
            r = int(random.uniform(0, len(cluster_data['x'])))
            myu.append([cluster_data['x'][r], cluster_data['y'][r], cluster_data['z'][r]])
        
        for k in range(100):
            clusters = get_clusters(myu, cluster_data, c_size)
            
#             print len(clusters[0]), len(clusters[1])
            
            new_myu = update_mu(clusters, c_size)
            
            same = True
            for m in myu:
                same = same and (m in new_myu)
                if not same:
                    break
            
            if same:
                print 'Seperated',k
#                 print myu
#                 print new_myu
                
                clu = clusters
                cols = ['#000000','#fffaaa']
                
                print len(clusters[0]), len(clusters[1]) 
                
                cluster = [[] for _ in range(c_size)]
                for j in range(c_size):
                    
                    for pts in clu[j]:
                        new_pts = [pts[0]**0.5, pts[1]**0.5]
                        for l in range(len(circle_data)):
                            if new_pts == [abs(circle_data['x'][l]), abs(circle_data['y'][l])]:
                                cluster[j].append([circle_data['x'][l], circle_data['y'][l]])
                                
                
                print len(cluster[0]), len(cluster[1])
                
                for j in range(c_size):  
                    c_t = numpy.array(cluster[j]).transpose()
                    plt.plot(c_t[0], c_t[1],'.', color = cols[j])
#                     col = str(random.random())
                
#                     ax.scatter(c_t[0],  c_t[1],c_t[2], color = cols[j])
                plt.show()
                            
                break
            else:
                myu = new_myu


blob_file_name = 'hw5_blob(1).csv'
circle_file_name = 'hw5_circle(1).csv' 

blob_data = []
circle_data = []

blob_data = pandas.read_csv(blob_file_name, sep = ',', names = ['x', 'y'])
circle_data =pandas.read_csv(circle_file_name, sep = ',', names = ['x', 'y'])
# 
new_data = DataFrame()
new_data['x'] = Series([circle_data['x'][i]**2  for i in range(len(circle_data['x']))])
new_data['y'] = Series([circle_data['y'][i]**2  for i in range(len(circle_data['x']))])
new_data['z'] = Series([circle_data['y'][i] **2 + circle_data['x'][i] ** 2  for i in range(len(circle_data['x']))])
# 
# 
perform_KMeans(blob_data, [2,3,5])
perform_KMeans(circle_data, [2,3,5])
# 
# ### Transform data
# 
perform_Kernel_KMeans(new_data)


loop = 0
final_iter = []
lls = []
min_means = []
min_log_value = -float('inf')
min_cov = []
min_weights = None
while(loop < 5):

    cov = numpy.cov(blob_data, rowvar = False)
    
    cluster_cov = [cov, cov, cov]
    cluster_alpha = [0.33,0.33,0.34]
    cluster_weights =[[],[],[]]
    cluster_means = []
    
    
    for k in range(3):
        r = int(random.uniform(0, len(blob_data['x'])))
        cluster_means.append([blob_data['x'][r],blob_data['y'][r]])
        #[,[blob_data['x'][1],blob_data['y'][1]],[blob_data['x'][2],blob_data['y'][2]]]
    
    
    iterations = []
    ll=[]
        
    l = 0
    for i in range(len(blob_data)):
        sum = 0
        for k in range(3):
            sum += cluster_alpha[k] * mn.pdf([blob_data['x'][i], blob_data['y'][i]], cluster_means[k], cluster_cov[k])
        l += math.log(sum)
    
    iterations.append(1)
    ll.append(l)
    
    iter = 0
    while (iter < 100):
        
        cluster_weights =[[],[],[]]
        
        for k in range(3):
            for i in range(len(blob_data)):
                cluster_weights[k].append(cluster_alpha[k] * mn.pdf([blob_data['x'][i], blob_data['y'][i]], cluster_means[k], cluster_cov[k]))
            
        for i in range(len(blob_data)):
            sum = cluster_weights[0][i] + cluster_weights[1][i] + cluster_weights[2][i]
            for k in range(3):
                cluster_weights[k][i] /= sum
                
        
        for k in range(3):
            cluster_alpha[k] = numpy.sum(cluster_weights[k]) / 600
        
    #     print cluster_alpha
        
        for k in range(3):
            x_sum = 0
            y_sum = 0
            for i in range(len(blob_data)):
                    x_sum += cluster_weights[k][i] * blob_data['x'][i] 
                    y_sum += cluster_weights[k][i] * blob_data['y'][i]
            
            n_k = numpy.sum(cluster_weights[k])
            cluster_means[k] = [x_sum / n_k, y_sum / numpy.sum(n_k) ]
        
        for k in range(3):
            a = [[0,0],[0,0]]
            
            for i in range(len(blob_data)):
                
                x = numpy.subtract([blob_data['x'][i], blob_data['y'][i]], cluster_means[k])
                
                mul_mat = numpy.multiply(cluster_weights[k][i] , numpy.dot(numpy.transpose(numpy.matrix(x)),numpy.matrix(x)))
                
                a = numpy.add(mul_mat, a)
            
            cluster_cov[k] = numpy.divide(a , numpy.sum(cluster_weights[k]))
            
        l_new = 0
        for i in range(len(blob_data)):
            sum = 0
            for k in range(3):
                sum += cluster_alpha[k] * mn.pdf([blob_data['x'][i], blob_data['y'][i]], cluster_means[k], cluster_cov[k])
            l_new += math.log(sum)
        
        if l_new >= l - 0.00001 and l_new <= l + 0.00001:
            
            if(min_log_value < l_new ):
                min_log_value = l_new
                min_means = cluster_means
                min_cov = cluster_cov
                min_weights = cluster_weights
                
            break
        
        l = l_new
        
        iterations.append(iter+1)
        ll.append(l)
            
        iter+=1
    
    loop+=1
    
    lls.append(ll)
    final_iter.append(iterations)
    
    plt.plot(iterations, ll)
    

print min_log_value
print min_cov
print min_means
plt.show()

final_clus = [[],[],[]]
# 
# for i in range(len(blob_data)):
#     print min_weights[0][i],min_weights[1][i],min_weights[2][i]
#     print max(min_weights[0][i],min_weights[1][i],min_weights[2][i])


for i in range(len(blob_data)):
    arr = numpy.array([min_weights[0][i],min_weights[1][i],min_weights[2][i]])
    index =  arr.argmax()
    final_clus[index].append([blob_data['x'][i], blob_data['y'][i]])

# 
# for i in range(len(blob_data)):
#     max = -float('inf')
#     label = None
#     for k in range(3):
#         if min_weights[k][i] > max:
#             max = min_weights[k][i]
#             label = k
#     final_clus[label].append([blob_data['x'][i], blob_data['y'][i]])
# 
# 
# 
for k in range(3):
    cl = numpy.array(final_clus[k]).transpose()
    plt.plot(cl[0],cl[1], '.')
  
plt.show()
